const zlib = require('zlib');

function gzipMaxCompression(buffer, options, done) {
  return zlib.gzip(buffer, { level: 9 }, done);
}

module.exports = { gzipMaxCompression };
