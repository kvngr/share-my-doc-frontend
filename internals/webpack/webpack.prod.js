const config = require('dotenv').config({
  path: '.env.production'
}).parsed;
const { client, server } = require('./webpack.common');
const { gzipMaxCompression } = require('../utils/gzipMaxCompression.js');
const { DefinePlugin } = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

// Server config
server.mode = 'production';
server.plugins.push(
  new DefinePlugin({
    'process.env': JSON.stringify({ ...config })
  })
);

// Client config
client.mode = 'production';
client.optimization = {
  minimizer: [new UglifyJsPlugin()]
};
client.plugins.push(
  new CompressionPlugin({
    algorithm: gzipMaxCompression
  }),
  new DefinePlugin({
    'process.env': JSON.stringify({
      PORT_CLIENT: config.PORT_CLIENT,
      PORT_API: config.PORT_API,
      VERSION_API: config.VERSION_API
    })
  })
);

module.exports = [client, server];
