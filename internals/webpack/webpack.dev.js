const { HotModuleReplacementPlugin, NamedModulesPlugin, DefinePlugin } = require('webpack');
const { client, server } = require('./webpack.common');
const { getIp } = require('../scripts/networkInterface');

const config = require('dotenv').config({
  path: '.env.development'
}).parsed;

const PORT_CLIENT = process.env.PORT_CLIENT || 3003;

// Server config
server.mode = 'development';
server.plugins.push(
  new DefinePlugin({
    'process.env': JSON.stringify({ ...config })
  })
);

server.watch = true;

// Client config
client.mode = 'development';
client.plugins.push(
  new NamedModulesPlugin(),
  new HotModuleReplacementPlugin(),
  new DefinePlugin({
    'process.env': JSON.stringify({
      PORT_CLIENT: config.PORT_CLIENT,
      PORT_API: config.PORT_API,
      VERSION_API: config.VERSION_API
    })
  })
);
client.devtool = 'inline-source-map';
client.performance = { hints: false };
client.devServer = {
  host: getIp(),
  port: PORT_CLIENT,
  compress: true,
  historyApiFallback: true,
  hot: true,
  open: true
};

module.exports = [client, server];
