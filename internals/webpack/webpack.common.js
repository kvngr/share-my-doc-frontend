const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { CheckerPlugin } = require('awesome-typescript-loader');
const path = require('path');

const ROOT_DIRECTORY = process.cwd();

const server = {
  name: 'server',
  target: 'node',
  entry: {
    server: './server/src/bin/www'
  },
  output: {
    path: path.resolve(ROOT_DIRECTORY, 'build', 'server'),
    filename: '[name].bundle.js'
  },
  plugins: [new CleanWebpackPlugin()]
};

const client = {
  name: 'client',
  target: 'web',
  context: ROOT_DIRECTORY,
  entry: ['./src/index.tsx'],
  output: {
    path: path.resolve(ROOT_DIRECTORY, 'build', 'client'),
    filename: '[name].bundle.js',
    publicPath: '/'
  },
  node: { fs: 'empty' },
  module: {
    rules: [
      {
        exclude: /(node_modules|bower_components)/,
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader',
        resolve: {
          extensions: ['*', '.js', '.jsx', '.ts', '.tsx']
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpg|pdf|gif|ico)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'assets/images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack']
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'assets/fonts/[name].[ext]'
          }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(ROOT_DIRECTORY, 'src', 'index.html'),
      favicon: path.resolve(ROOT_DIRECTORY, 'public', 'images', 'favicon.ico')
    }),
    new CheckerPlugin(),
    new CleanWebpackPlugin()
  ],
  resolve: {
    plugins: [new TsConfigPathsPlugin({ configFile: './tsconfig.json' })],
    extensions: ['.mjs', '.js', '.jsx', '.ts', '.tsx'],
    alias: {
      components: path.resolve(ROOT_DIRECTORY, 'src', 'components')
    }
  }
};

module.exports = {
  client,
  server
};
