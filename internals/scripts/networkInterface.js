const ni = require('os').networkInterfaces();

function getIp() {
  let ip = Object.keys(ni)
    .map(interf => ni[interf].map(o => !o.internal && o.family === 'IPv4' && o.address))
    .reduce((a, b) => a.concat(b))
    .filter(o => o)[0];
  return ip;
}

module.exports = {
  getIp
};
