const exec = require('child_process').exec;
const chalk = require('chalk');

function checkNodeVersion() {
  return new Promise(resolve => {
    exec('node -v', (err, stdout) => {
      const nodeVersion = 10;
      stdout = process.versions.node;
      if (err) throw err;
      if (parseFloat(stdout) < nodeVersion) {
        throw new Error(`[ERROR] You need node version @>=${nodeVersion}`).process.exit(1);
      }
      resolve(console.log(chalk.green('node version Ok ✔︎')));
    });
  });
}

function checkNpmVersion() {
  return new Promise(resolve => {
    exec('npm -v', (err, stdout) => {
      const npmVersion = 5;
      if (err) throw err;
      if (parseFloat(stdout) < npmVersion) {
        throw new Error(`[ERROR] You need npm version @>=${npmVersion}`).process.exit(1);
      }
      resolve(console.log(chalk.green('npm version Ok ✔︎')));
    });
  });
}

function allChecked() {
  return Promise.all([checkNodeVersion(), checkNpmVersion()]);
}

async function waitChecking() {
  console.log(chalk.yellow('checking Node environment ...'));
  let result = await allChecked();
  return result;
}

waitChecking();
