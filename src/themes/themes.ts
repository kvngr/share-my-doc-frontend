import { DefaultTheme } from 'styled-components';

import { colors } from './colors';

export const lightTheme: DefaultTheme = { color: colors.blackBlue, backgroundColor: colors.white };
export const darkTheme: DefaultTheme = { color: colors.white, backgroundColor: colors.blackBlue };
