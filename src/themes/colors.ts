export const colors = {
  black: '#000000',
  blackBlue: '#282c35',
  lightGrey: '#E7E7E7',
  dawnGrey: '#c6c6c6',
  white: '#FFFFFF',
  cloudGrey: '#9fabbb',
  error: '#ef4850',
  info: '#bae1fd',
  primary: '#0066CC',
  blue: '#1e88e5',
  green: '#4caf50',
  red: '#f44336',
  grey: '#9e9e9e',
  secondary: '#FF6633',
  success: '#a2efbd',
  warning: '#f9e07d'
};
