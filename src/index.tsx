import * as React from 'react';
import { render } from 'react-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import { BrowserRouter as Router } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';
import { setConfig } from 'react-hot-loader';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { client } from './core/graphql/config';

setConfig({
  pureRender: true,
  pureSFC: true
});

import App from './components/App';

const MOUNT_NODE = document.getElementById('app');

const renderApp = (Component: any) => {
  render(
    <ApolloProvider client={client}>
      <Router>
        <Component />
      </Router>
    </ApolloProvider>,
    MOUNT_NODE
  );
};

renderApp(App);

library.add(fas);

if (process.env.NODE_ENV === 'development') {
  hot(App);
}
