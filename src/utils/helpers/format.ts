import { format as fnsFormat } from 'date-fns';

export function dateFormat(
  value: string,
  format: string = 'dd/MM/yyyy à HH:mm',
  options?: {}
): string {
  const formattedValue = Number(value);
  const date = new Date(formattedValue);
  return fnsFormat(date, format, options);
}

export function getStringFromArray(array: any[] = [], key: string): string {
  const list: string[] = [];

  if (!Array.isArray(array) || typeof key !== 'string')
    throw new Error('🥶 Error you must pass a valid array and valid key as string');

  array.forEach(element => {
    list.push(element[key]);
  });

  const formattedValue = list.join(', ');

  return formattedValue;
}
