import { IconLookup, IconDefinition, findIconDefinition } from '@fortawesome/fontawesome-svg-core';

const userLookup: IconLookup = { prefix: 'fas', iconName: 'user' };
const homeLookup: IconLookup = { prefix: 'fas', iconName: 'home' };
const signInLookup: IconLookup = { prefix: 'fas', iconName: 'sign-in-alt' };
const dashboardLookup: IconLookup = { prefix: 'fas', iconName: 'tachometer-alt' };
const adminLookup: IconLookup = { prefix: 'fas', iconName: 'user-shield' };
const signOutLookup: IconLookup = { prefix: 'fas', iconName: 'sign-out-alt' };
const trashLookup: IconLookup = { prefix: 'fas', iconName: 'trash' };
const eyeLookup: IconLookup = { prefix: 'fas', iconName: 'eye' };
const editLookup: IconLookup = { prefix: 'fas', iconName: 'edit' };
const circleCloseLookup: IconLookup = { prefix: 'fas', iconName: 'times-circle' };
const addLookup: IconLookup = { prefix: 'fas', iconName: 'plus' };

const userIcon: IconDefinition = findIconDefinition(userLookup);
const homeIcon: IconDefinition = findIconDefinition(homeLookup);
const signInIcon: IconDefinition = findIconDefinition(signInLookup);
const dashboardIcon: IconDefinition = findIconDefinition(dashboardLookup);
const adminIcon: IconDefinition = findIconDefinition(adminLookup);
const logoutIcon: IconDefinition = findIconDefinition(signOutLookup);
const trashIcon: IconDefinition = findIconDefinition(trashLookup);
const eyeIcon: IconDefinition = findIconDefinition(eyeLookup);
const editIcon: IconDefinition = findIconDefinition(editLookup);
const circleCloseIcon: IconDefinition = findIconDefinition(circleCloseLookup);
const addIcon: IconDefinition = findIconDefinition(addLookup);

export {
  userIcon,
  homeIcon,
  signInIcon,
  dashboardIcon,
  adminIcon,
  logoutIcon,
  trashIcon,
  addIcon,
  eyeIcon,
  editIcon,
  circleCloseIcon
};
