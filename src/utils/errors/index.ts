export const errorsList: any = {
  generic: {
    emptyField: 'Veuillez compléter ce champ',
    incompleteField: 'Il manque des champs'
  },
  input: {
    email: 'Adresse email non valide',
    firstName: 'Prénom non valide',
    lastName: 'Nom non valide',
    password: {
      atLeastOneDigit: '1 chiffre',
      atLeastOneLowerCase: '1 minuscule,',
      atLeastOneSpecialCharacter: 'et 1 caractère spécial.',
      atLeastOneUpperCase: '1 majuscule,',
      // notMatch: 'Les mots de passes de correspondent pas',
      tooShort: '8 caractères minimum,'
    }
  }
};

export default errorsList;
