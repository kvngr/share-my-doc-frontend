import { UserRole } from 'src/core/types/user';

interface IValidator {
  valueIsFilled(value: string): boolean;
  imageIsValid(file: any): boolean;
  emailIsValid(value: string): boolean;
  passwordIsTooShort(value: string): boolean;
  passwordHasOneLowerCase(value: string): boolean;
  passwordHasOneUpperCase(value: string): boolean;
  passwordHasOneDigit(value: string): boolean;
  passwordHasOneSpecialCharacter(value: string): boolean;
  passwordAreSame(a: string, b: string): boolean;
}

export class Validator implements IValidator {
  public valueIsFilled(value: string) {
    return value !== '';
  }

  public nameIsValid(value: string) {
    const regex = /[a-zA-Z]+/g;
    return regex.test(value);
  }

  public imageIsValid(file: any) {
    const regex = /\.(jpe?g|png|gif)$/i;
    return regex.test(file.name);
  }

  public emailIsValid(value: string) {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(value));
  }

  /** BEGIN PASSWORD VALIDATION */
  public passwordIsvalid(value: string) {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/;
    return regex.test(value);
  }

  public passwordIsTooShort(value: string) {
    const regex = /^(?=.{8,})+/g;
    return regex.test(value);
  }

  public passwordHasOneLowerCase(value: string) {
    const regex = /^(?=.*[a-z])+/g;
    return regex.test(value);
  }

  public passwordHasOneUpperCase(value: string) {
    const regex = /^(?=.*[A-Z])+/g;
    return regex.test(value);
  }

  public passwordHasOneDigit(value: string) {
    const regex = /^(?=.*[0-9])+/g;
    return regex.test(value);
  }

  public passwordHasOneSpecialCharacter(value: string) {
    const regex = /^(?=.*[!@#\$%\^&\*])+/g;
    return regex.test(value);
  }

  public passwordAreSame(a: string, b: string) {
    return a === b;
  }

  public roleIsValid(value: string) {
    return value === typeof UserRole;
  }
  /** END PASSWORD VALIDATION */
}

export default Validator;
