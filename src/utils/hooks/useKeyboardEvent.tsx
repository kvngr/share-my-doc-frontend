import { useEffect } from 'react';

export function useKeyboardEvent(key: string, callback: any) {
  useEffect(() => {
    const handler = (event: any) => {
      if (event.key === key) {
        callback();
      }
    };
    window.addEventListener('keydown', handler);
    return () => {
      window.removeEventListener('keydown', handler);
    };
  }, [callback, key]);
}
