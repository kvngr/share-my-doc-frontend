import { useEffect } from 'react';
import { Ref } from 'react-hook-form/dist/types';

export function useOnClickOutside<T>(ref: Ref, handler: T) {
  useEffect(() => {
    const listener = (event: Event) => {
      // Do nothing if clicking ref's element or descendent elements
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      if (handler && typeof handler === 'function') {
        handler();
      }
    };

    window.addEventListener('mousedown', listener);
    window.addEventListener('touchstart', listener);

    return () => {
      window.removeEventListener('mousedown', listener);
      window.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
}
