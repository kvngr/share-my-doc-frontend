import { SetState, GetState } from 'zustand';

export type Actions<S> = (set: SetState<S>, get: GetState<S>) => { [key: string]: (...args: any[]) => void };

export type Store<S, A extends Actions<S>> = S & { actions: ReturnType<A> };
