import { useUIStore, selectUIIsLoading } from './ui';
import { useAuthStore, selectAuthIsLoading, selectAuthIsLoaded } from './auth';

export const useAppIsLoading = () => {
  const isUILoading = useUIStore(selectUIIsLoading);
  const isAuthLoading = useAuthStore(selectAuthIsLoading);

  return isUILoading || isAuthLoading;
};

export const useAppIsLoaded = () => {
  const isAuthLoaded = useAuthStore(selectAuthIsLoaded);

  return isAuthLoaded;
};
