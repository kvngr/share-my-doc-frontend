import { create } from 'zustand';

import { Store } from '../types';
import { DocumentState, IntitialDocumentState } from './document.state';
import { documentActions } from './document.actions';

export type DocumentStore = Store<DocumentState, typeof documentActions>;

export const [useDocumentStore] = create<DocumentStore>((set, get) => ({
  ...IntitialDocumentState,
  actions: documentActions(set, get)
}));
