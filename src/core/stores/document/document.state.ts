import { Document, Documents } from 'src/core/types/document';

export interface DocumentState {
  documents: Documents[];
  document: Document | null;
  isDeleted?: boolean;
  isUploaded?: boolean;
  successUploaded?: boolean;
  isUpdated?: boolean;
  isLoaded: boolean;
  isLoading: boolean;
  error?: any;
}

export const IntitialDocumentState: DocumentState = {
  documents: [],
  document: null,
  isDeleted: false,
  isUpdated: false,
  isUploaded: false,
  successUploaded: false,
  isLoaded: false,
  isLoading: false,
  error: null
};
