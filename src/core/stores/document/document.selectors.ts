import { useDocumentStore } from './document.store';
import { DocumentState } from './document.state';

export const selectDocumentIsLoading = (state: DocumentState) => state.isLoading;
export const selectDocumentIsLoaded = (state: DocumentState) => state.isLoaded;
