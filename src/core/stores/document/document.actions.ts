import gql from 'graphql-tag';
import { DocumentState } from './document.state';
import { Actions } from '../types';
import { SetState, GetState } from 'zustand';
import { Document } from 'src/core/types/document';
import { client } from 'src/core/graphql/config';

export const CREATE_DOCUMENT_MUTATION = gql`
  mutation CreateDocument($document: CreateDocumentInput!) {
    createDocument(document: $document) {
      id
      title
      content
      revision
      version
      status
    }
  }
`;

export const UPLOAD_DOCUMENT_MUTATION = gql`
  mutation UploadDocument($document: Upload!) {
    uploadDocument(document: $document) {
      title
      content
      rules {
        title
        content
        image
      }
    }
  }
`;

export const UPDATE_DOCUMENT_MUTATION = gql`
  mutation UpdateDocument($id: ID!, $name: String, $description: String) {
    updateDomain(domain: { id: $id, name: $name, description: $description }) {
      id
      title
      content
      status
      ruleIds
      domainId
      revision
      version
    }
  }
`;

export const DELETE_DOCUMENT_MUTATION = gql`
  mutation DeleteDocument($id: ID!) {
    deleteDocument(document: { id: $id })
  }
`;

export const FIND_ONE_DOCUMENT_QUERY = gql`
  query FindOneDocument($id: ID!) {
    document(id: $id) {
      id
      title
      content
      status
    }
  }
`;

export const FIND_DOCUMENTS_QUERY = gql`
  query GetDocuments($title: String) {
    documents(title: $title) {
      id
      title
      content
    }
  }
`;

export const documentActions: Actions<DocumentState> = (
  set: SetState<DocumentState>,
  get: GetState<DocumentState>
) => ({
  createDocument: async (document: Document) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: CREATE_DOCUMENT_MUTATION,
        variables: document,
        refetchQueries: [{ query: FIND_DOCUMENTS_QUERY }]
      });
      if (data) {
        const { createDocument: document } = data;
        set({ document });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  uploadDocument: async (document: any) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: UPLOAD_DOCUMENT_MUTATION,
        variables: { document }
      });
      if (data) {
        const { uploadDocument: document } = data;
        set({ document });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  findOneDocument: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.query({ query: FIND_ONE_DOCUMENT_QUERY, variables: id });
      if (data) {
        const { document } = data;
        set({ document });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  updateDocument: async (id: string, content: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: UPDATE_DOCUMENT_MUTATION,
        variables: { id, content },
        refetchQueries: [{ query: FIND_DOCUMENTS_QUERY }]
      });
      if (data) {
        const { updateDocument: document } = data;
        set({ document });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  deleteDocument: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: DELETE_DOCUMENT_MUTATION,
        variables: id,
        refetchQueries: [{ query: FIND_DOCUMENTS_QUERY }]
      });
      if (data) {
        const { deleteDocument } = data;
        set({ isDeleted: !!deleteDocument });
        set({ document: null });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  findDocuments: async (title?: string) => {
    set({ isLoaded: false });
    try {
      const { data } = await client.query({ query: FIND_DOCUMENTS_QUERY, variables: title });
      if (data) {
        const { documents } = data;
        set({ documents });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoaded: true });
    }
  }
});
