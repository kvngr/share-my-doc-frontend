import { SetState, GetState } from 'zustand';
import { client } from 'src/core/graphql/config';
import { Actions } from '../types';
import gql from 'graphql-tag';
import { RuleState } from './rule.state';
import { Rule } from 'src/core/types/rule';

const CREATE_RULE_MUTATION = gql`
  mutation CreateRule($rule: CreateRuleInput!) {
    createRule(rule: $rule) {
      id
      title
      image
    }
  }
`;

const UPDATE_RULE_MUTATION = gql`
  mutation UpdateRule($rule: UpdateRuleInput!) {
    updateRule(rule: $rule) {
      id
    }
  }
`;

const FIND_RULES_QUERY = gql`
  query GetRules($id: String) {
    rules(id: $id) {
      id
      title
      # domain {
      #   name
      # }
    }
  }
`;

const FIND_ONE_RULE = gql`
  query GetRule($id: ID!) {
    rule(id: $id) {
      id
      title
    }
  }
`;

const DELETE_ONE_RULE = gql`
  mutation DeleteRule($rule: DeleteRuleInput!) {
    deleteRule(rule: $rule)
  }
`;

export const ruleActions: Actions<RuleState> = (
  set: SetState<RuleState>,
  get: GetState<RuleState>
) => ({
  createRule: async (rule: Rule) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: CREATE_RULE_MUTATION,
        variables: rule,
        refetchQueries: [{ query: FIND_RULES_QUERY }]
      });
      if (data) {
        const { createRule: rule } = data;
        set({ rule });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  updateRule: async (
    id: string,
    title: string,
    content: string,
    domainId: string,
    image: string,
    major: boolean
  ) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: UPDATE_RULE_MUTATION,
        variables: { id, title, content, domainId, image, major },
        refetchQueries: [{ query: FIND_RULES_QUERY }]
      });
      if (data) {
        const { updateRule: rule } = data;
        set({ rule });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  findRules: async (id: string) => {
    set({ isLoaded: false });
    try {
      const { data } = await client.query({ query: FIND_RULES_QUERY, variables: id });
      if (data) {
        const { rules } = data;
        set({ rules });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoaded: true });
    }
  },
  findOneRule: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.query({ query: FIND_ONE_RULE, variables: id });
      if (data) {
        const { rule } = data;
        set({ rule });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  deleteOneRule: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: DELETE_ONE_RULE,
        variables: id,
        refetchQueries: [{ query: FIND_RULES_QUERY }]
      });
      if (data) {
        const { deleteRule } = data;
        set({ isDeleted: !!deleteRule });
        set({ rule: null });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  }
});
