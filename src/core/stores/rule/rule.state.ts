import { Rule, Rules } from 'src/core/types/rule';

export interface RuleState {
  rules: Rules[];
  rule: Rule | null;
  isLoaded: boolean;
  isLoading: boolean;
  isDeleted: boolean;
  error?: any;
}

export const IntitialRuleState: RuleState = {
  rules: [],
  rule: null,
  isLoaded: false,
  isDeleted: false,
  isLoading: false,
  error: null
};
