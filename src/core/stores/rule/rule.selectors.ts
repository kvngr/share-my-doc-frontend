import { useRuleStore } from './rule.store';
import { RuleState } from './rule.state';

export const selectRuleIsLoading = (state: RuleState) => state.isLoading;
export const selectRuleIsLoaded = (state: RuleState) => state.isLoaded;
