import { create } from 'zustand';

import { Store } from '../types';
import { RuleState, IntitialRuleState } from './rule.state';
import { ruleActions } from './rule.actions';

export type RuleStore = Store<RuleState, typeof ruleActions>;

export const [useRuleStore] = create<RuleStore>((set, get) => ({
  ...IntitialRuleState,
  actions: ruleActions(set, get)
}));
