import { create } from 'zustand';

import { Store } from '../types';
import { UserState, IntitialUserState } from './user.state';
import { userActions } from './user.actions';

export type UserStore = Store<UserState, typeof userActions>;

export const [useUserStore] = create<UserStore>((set, get) => ({
  ...IntitialUserState,
  actions: userActions(set, get)
}));
