import { SetState, GetState } from 'zustand';
import { client } from 'src/core/graphql/config';
import { Actions } from '../types';
import gql from 'graphql-tag';
import { UserState } from './user.state';
import { User } from 'src/core/types/user';

const FIND_USERS_QUERY = gql`
  query FindUsers($email: String) {
    users(email: $email) {
      id
      firstName
      lastName
      email
      role
    }
  }
`;

const CREATE_USER_MUTATION = gql`
  mutation CreateUser($user: CreateUserInput!) {
    createUser(user: $user) {
      id
      email
      role
      firstName
      lastName
      createdAt
      updatedAt
      domain {
        id
        name
      }
      allowedDomains {
        id
        name
      }
    }
  }
`;

const FIND_ONE_USER_QUERY = gql`
  query FindOneUser($id: ID!) {
    user(id: $id) {
      id
      email
      role
      firstName
      lastName
      createdAt
      updatedAt
      domain {
        id
        name
      }
      allowedDomains {
        id
        name
      }
    }
  }
`;

const UPDATE_USER_MUTATION = gql`
  mutation UpdateUser($id: ID!, $firstName: String, $lastName: String) {
    updateUser(user: { id: $id, firstName: $firstName, lastName: $lastName }) {
      id
      role
      email
      firstName
      lastName
    }
  }
`;

const DELETE_USER_MUTATION = gql`
  mutation DeleteUser($id: ID!) {
    deleteUser(user: { id: $id })
  }
`;

export const userActions: Actions<UserState> = (
  set: SetState<UserState>,
  get: GetState<UserState>
) => ({
  findUsers: async (email?: string) => {
    set({ isLoaded: false });
    try {
      const { data } = await client.query({ query: FIND_USERS_QUERY, variables: email });
      if (data) {
        const { users } = data;
        set({ users });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoaded: true });
    }
  },
  createUser: async (user: User) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: CREATE_USER_MUTATION,
        variables: user,
        refetchQueries: [{ query: FIND_USERS_QUERY }]
      });
      if (data) {
        const { createUser: user } = data;
        set({ user });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  findOneUser: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.query({ query: FIND_ONE_USER_QUERY, variables: id });
      if (data) {
        const { user } = data;
        set({ user });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  updateUser: async (id: string, firstName: string, lastName: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: UPDATE_USER_MUTATION,
        variables: { id, firstName, lastName },
        refetchQueries: [{ query: FIND_USERS_QUERY }]
      });
      if (data) {
        const { updateUser: user } = data;
        set({ user });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  deleteOneUser: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: DELETE_USER_MUTATION,
        variables: id,
        refetchQueries: [{ query: FIND_USERS_QUERY }]
      });
      if (data) {
        const { deleteUser } = data;
        set({ isDeleted: !!deleteUser });
        set({ user: null });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  }
});
