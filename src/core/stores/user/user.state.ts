import { User } from 'src/core/types/user';

export interface UserState {
  users: User[];
  user: User | null;
  isDeleted?: boolean;
  isUpdated?: boolean;
  isLoaded: boolean;
  isLoading: boolean;
  error?: any;
}

export const IntitialUserState: UserState = {
  users: [],
  user: null,
  isDeleted: false,
  isUpdated: false,
  isLoaded: false,
  isLoading: false,
  error: null
};
