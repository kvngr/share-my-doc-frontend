import { useUserStore } from './user.store';
import { UserState } from './user.state';

export const selectUserIsLoading = (state: UserState) => state.isLoading;
export const selectUserIsLoaded = (state: UserState) => state.isLoaded;
