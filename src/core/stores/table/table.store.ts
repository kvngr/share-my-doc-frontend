import create from 'zustand';

export const [useTableStore] = create((set, get) => ({
  rowId: null,
  setRowId: (rowId: string) => set(() => rowId),
  resetRowId: () => set(() => ({ rowId: null }))
}));
