import { AuthState } from './auth.state';

export const selectAuthIsAuthenticated = (state: AuthState) => !!state.token;

export const selectAuthIsLoading = (state: AuthState) => state.isLoading;

export const selectAuthIsLoaded = (state: AuthState) => state.isLoaded;
