import { create, GetState, SetState } from 'zustand';

import { Actions, Store } from '../types';
import { AuthState, authInitialState } from './auth.state';
import { authActions } from './auth.actions';

export type AuthStore = Store<AuthState, typeof authActions>;

export const [useAuthStore] = create<AuthStore>((set, get) => ({
  ...authInitialState,
  actions: authActions(set, get)
}));
