import { User } from 'src/core/types/user';

export interface AuthState {
  isLoaded: boolean;
  isLoading: boolean;
  token: string | null;
  // isAuthenticated: boolean;
  error?: any;
  user: null | User;
}

export const authInitialState: AuthState = {
  isLoaded: false,
  isLoading: false,
  token: null,
  user: null
  // isAuthenticated: false
};
