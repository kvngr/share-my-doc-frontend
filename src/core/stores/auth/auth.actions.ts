import { AUTH_TOKEN } from './../../constants/index';
import { SetState, GetState } from 'zustand';
import gql from 'graphql-tag';

import { client } from 'src/core/graphql/config';
import { Actions } from '../types';
import { AuthState } from './auth.state';
import { User, Credentials } from 'src/core/types/user';

export const AUTHENTICATE_QUERY = gql`
  {
    authenticate {
      token
      user {
        id
        email
        role
        firstName
        lastName
        createdAt
        updatedAt
        domain {
          id
          name
        }
        allowedDomains {
          id
          name
        }
      }
    }
  }
`;

export interface AuthenticateQueryResponse {
  authenticate: { token: string; user: User };
}

export const SIGN_IN_MUTATION = gql`
  mutation SignIn($credentials: SignInInput!) {
    signIn(credentials: $credentials) {
      token
      user {
        id
        email
        role
        firstName
        lastName
        createdAt
        updatedAt
        domain {
          id
          name
        }
        allowedDomains {
          id
          name
        }
      }
    }
  }
`;

export interface SignInMutationResponse {
  signIn: {
    user: User;
    token: string;
  };
}

export const authActions: Actions<AuthState> = (
  set: SetState<AuthState>,
  get: GetState<AuthState>
) => ({
  authenticate: async () => {
    set({ isLoaded: false });
    try {
      const { data } = await client.query<AuthenticateQueryResponse>({ query: AUTHENTICATE_QUERY });
      if (data) {
        const {
          authenticate: { user, token }
        } = data;
        set({ user, token });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoaded: true });
    }
  },
  signIn: async (credentials: Credentials) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate<SignInMutationResponse>({
        mutation: SIGN_IN_MUTATION,
        variables: { credentials }
      });
      if (!data) {
        return set({ error: new Error('An error occured') });
      }
      const {
        signIn: { token, user }
      } = data;
      localStorage.setItem(AUTH_TOKEN, token);
      set({ token, user });
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  logout: () => {
    set({ isLoading: true });
    localStorage.removeItem(AUTH_TOKEN);
    set({ isLoading: false, token: null });
    set({ user: null });
  }
});
