import { create, GetState, SetState } from 'zustand';

import { Actions, Store } from '../types';
import { UIState, uiInitialState } from './ui.state';
import { uiActions } from './ui.actions';

export type UIStore = Store<UIState, typeof uiActions>;

export const [useUIStore] = create<UIStore>((set, get) => ({
  ...uiInitialState,
  actions: uiActions(set, get)
}));
