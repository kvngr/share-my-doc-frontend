import { lightTheme } from '@themes';
import { DefaultTheme } from 'styled-components';
import { GetState, SetState } from 'zustand';

import { Actions } from '../types';
import { UIState } from './ui.state';

export const uiActions: Actions<UIState> = (set: SetState<UIState>, get: GetState<UIState>) => ({
  toggleMenu: () => set((state: UIState) => ({ isMenuOpen: !state.isMenuOpen })),
  openMenu: () => set({ isMenuOpen: true }),
  closeMenu: () => set({ isMenuOpen: false }),
  setTheme: (theme: DefaultTheme) => {
    const value = theme === lightTheme ? 'light' : 'dark';
    localStorage.setItem('theme', value);
    set({ theme, value });
  }
});
