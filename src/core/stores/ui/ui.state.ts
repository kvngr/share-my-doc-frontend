import { DefaultTheme } from 'styled-components';

import { lightTheme } from '../../../themes/themes';

export interface UIState {
  isMenuOpen: boolean;
  theme: DefaultTheme;
  isLoading: boolean;
  value: string;
}

export const uiInitialState: UIState = {
  isMenuOpen: false,
  theme: lightTheme,
  isLoading: false,
  value: 'light'
};
