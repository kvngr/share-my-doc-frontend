import { UIState } from './ui.state';

export const selectUIIsLoading = (state: UIState) => state.isLoading;
