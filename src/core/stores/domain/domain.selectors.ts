import { useDomainStore } from './domain.store';
import { DomainState } from './domain.state';

export const selectDomainIsLoading = (state: DomainState) => state.isLoading;
export const selectDomainIsLoaded = (state: DomainState) => state.isLoaded;
