import { create } from 'zustand';

import { Store } from '../types';
import { DomainState, IntitialDomainState } from './domain.state';
import { domainActions } from './domain.actions';

export type DomainStore = Store<DomainState, typeof domainActions>;

export const [useDomainStore] = create<DomainStore>((set, get) => ({
  ...IntitialDomainState,
  actions: domainActions(set, get)
}));
