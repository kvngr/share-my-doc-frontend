import { SetState, GetState } from 'zustand';
import { client } from 'src/core/graphql/config';
import { Actions } from '../types';
import gql from 'graphql-tag';
import { DomainState } from './domain.state';

const CREATE_DOMAIN_MUTATION = gql`
  mutation CreateDomain($name: String!, $description: String) {
    createDomain(domain: { name: $name, description: $description }) {
      id
      name
      description
    }
  }
`;

const UPDATE_DOMAIN_MUTATION = gql`
  mutation UpdateDomain($id: ID!, $name: String, $description: String) {
    updateDomain(domain: { id: $id, name: $name, description: $description }) {
      id
      name
      description
    }
  }
`;

const FIND_DOMAINS_QUERY = gql`
  query FindDomains($name: String) {
    domains(name: $name) {
      id
      name
      description
    }
  }
`;

const FIND_ONE_DOMAIN = gql`
  query FindOneDomain($id: ID!) {
    domain(id: $id) {
      id
      name
      description
    }
  }
`;

const DELETE_ONE_DOMAIN = gql`
  mutation DeleteDomain($id: ID!) {
    deleteDomain(domain: { id: $id })
  }
`;

export const domainActions: Actions<DomainState> = (
  set: SetState<DomainState>,
  get: GetState<DomainState>
) => ({
  createDomain: async (name: string, description?: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: CREATE_DOMAIN_MUTATION,
        variables: { name, description },
        refetchQueries: [{ query: FIND_DOMAINS_QUERY }]
      });
      if (data) {
        const { createDomain } = data;
        set({ domain: createDomain });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  updateDomain: async (id: string, name?: string, description?: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: UPDATE_DOMAIN_MUTATION,
        variables: { id, name, description },
        refetchQueries: [{ query: FIND_DOMAINS_QUERY }]
      });
      if (data) {
        const { updateDomain: domain } = data;
        set({ domain });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  findDomains: async (name: string) => {
    set({ isLoaded: false });
    try {
      const { data } = await client.query({ query: FIND_DOMAINS_QUERY, variables: name });
      if (data) {
        const { domains } = data;
        set({ domains });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoaded: true });
    }
  },
  findOneDomain: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.query({ query: FIND_ONE_DOMAIN, variables: id });
      if (data) {
        const { domain } = data;
        set({ domain });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  },
  deleteOneDomain: async (id: string) => {
    set({ isLoading: true });
    try {
      const { data } = await client.mutate({
        mutation: DELETE_ONE_DOMAIN,
        variables: id,
        refetchQueries: [{ query: FIND_DOMAINS_QUERY }]
      });
      if (data) {
        const { deleteDomain } = data;
        set({ isDeleted: !!deleteDomain });
        set({ domain: null });
      }
    } catch (error) {
      set({ error });
    } finally {
      set({ isLoading: false });
    }
  }
});
