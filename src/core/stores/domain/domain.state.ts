import { Domain, Domains } from 'src/core/types/domain';

export interface DomainState {
  domains: Domains[];
  domain: Domain | null;
  isLoaded: boolean;
  isLoading: boolean;
  isDeleted: boolean;
  error?: any;
}

export const IntitialDomainState: DomainState = {
  domains: [],
  domain: null,
  isLoaded: false,
  isDeleted: false,
  isLoading: false,
  error: null
};
