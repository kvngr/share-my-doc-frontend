import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { createUploadLink } from 'apollo-upload-client';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { setContext } from 'apollo-link-context';

import { environment } from '../../../environment/env.dev';
import { AUTH_TOKEN } from './../constants/index';

const credentials = 'same-origin';
const uri = `http://${environment.server.host}:${environment.server.port}`;

export interface AuthPayload {
  id: null | string;
  email: null | string;
  token: null | string;
}

const cache = new InMemoryCache();

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem(AUTH_TOKEN);
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  };
});

const uploadLink = createUploadLink({ uri, credentials });

// const httpLink = new HttpLink({
//   credentials,
//   uri
// });

export const client = new ApolloClient({
  cache,
  link: ApolloLink.from([
    authLink,
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors)
        graphQLErrors.forEach(({ message, locations, path }) =>
          console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
        );
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    uploadLink
    // httpLink
  ])
});
