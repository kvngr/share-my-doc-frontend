import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { selectAuthIsAuthenticated } from '../stores/auth/auth.selectors';
import { useAuthStore } from '../stores';
// import { AuthContext } from '../stores/auth/auth.context';

interface PrivateRouteProps {
  component: any;
  exact?: boolean;
  path: string;
  redirectTo?: string;
}

export const PrivateRoute = ({ component: Component, ...rest }: PrivateRouteProps) => {
  const isAuthenticated = useAuthStore(selectAuthIsAuthenticated);

  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated === true ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};
