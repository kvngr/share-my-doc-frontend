import React, { ReactNode } from 'react';
import { useLocation, Switch, Route } from 'react-router-dom';
import { useTransition, animated } from 'react-spring';
import { animatedRoutes } from 'src/components/GlobalStyle';
import { Loader } from '../../components/atoms/Loader';
// import { PrivateRoute } from './PrivateRoute';
// import { PublicRoute } from './PublicRoute';
import { useAuthStore } from '../stores';
import { PrivateRoute } from './PrivateRoute';

// const Home = React.lazy(() => import('../../components/pages/Home'));
const Login = React.lazy(() => import('../../components/pages/Login'));
const Profile = React.lazy(() => import('../../components/pages/Profile'));
const Dashboard = React.lazy(() => import('../../components/pages/Dashboard'));
// const Document = React.lazy(() => import('../../components/pages/Document'));

const AnimatedSwitch = ({ children }: { children: ReactNode }) => {
  const location = useLocation();
  const transitions = useTransition(location, (loc: any) => loc.pathname, { ...animatedRoutes });

  return (
    <>
      {transitions.map(({ item, props, key }) => (
        <animated.div key={key} style={{ ...props }}>
          <Switch location={item}>
            <React.Suspense fallback={<Loader />}>{children}</React.Suspense>
          </Switch>
        </animated.div>
      ))}
    </>
  );
};

export const Routes = () => {
  const { user } = useAuthStore();

  // console.log('TCL: Routes -> user', user);

  return (
    <AnimatedSwitch>
      <Route path="/login" component={Login} />
      <PrivateRoute path="/dashboard" component={Dashboard} />
      <PrivateRoute path="/profile" component={Profile} />
    </AnimatedSwitch>
  );
};
