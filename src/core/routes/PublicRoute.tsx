import React, { ReactNode } from 'react';
import { Route } from 'react-router-dom';
// import { useRedirect } from '../stores/auth/auth.selectors';

interface PublicRouteProps {
  component?: any;
  exact?: boolean;
  path: string;
  redirectTo?: string;
  children?: ReactNode;
}

export const PublicRoute = ({
  component: Component,
  exact,
  redirectTo = '/profile',
  path,
  children,
  ...rest
}: PublicRouteProps) => {
  // useRedirect();
  return (
    <>
      {children ? (
        <Route {...rest} exact={exact} path={path}>
          {children}
        </Route>
      ) : (
        <Route {...rest} exact={exact} path={path} render={props => <Component {...props} />} />
      )}
    </>
  );
};
