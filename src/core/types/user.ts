import { Domain } from './domain';

export enum UserRole {
  ADMIN = 'ADMIN',
  USER = 'USER'
}

export interface Credentials {
  email: string;
  password: string;
}

export interface User {
  id?: string;
  email: string;
  role: UserRole;
  firstName: string;
  lastName: string;
  createdAt?: string;
  updatedAt?: string;
  __typename?: string;
  domain: Domain;
  allowedDomains: Domain[];
}

export interface DeleteUser {
  id?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
}
