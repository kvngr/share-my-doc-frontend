export interface Document {
  title: string;
  content: string;
  ruleIds: string[];
  domainId: string;
  revision: number;
  version: number;
}

export interface Documents {
  documents: Document[];
}
