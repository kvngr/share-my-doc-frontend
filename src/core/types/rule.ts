export interface Rule {
  title: string;
  content: string;
  version: number;
  revision: number;
  domainId: string;
  image: string;
}

export interface Rules {
  rules: Rule[];
}
