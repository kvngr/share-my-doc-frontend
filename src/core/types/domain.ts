export interface Domain {
  id: string;
  description: string;
  name: string;
  __typename: string;
}

export interface Domains {
  domains: Domain[];
}
