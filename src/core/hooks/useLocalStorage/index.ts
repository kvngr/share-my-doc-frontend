import React from 'react';

interface IStorage {
  storedValue: string;
  setStoredValue: () => void;
}

export function useLocalStorage(key: string, initialValue?: string | null) {
  const [storedValue, setStoredValue] = React.useState<IStorage>(() => {
    try {
      const item = localStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      console.log(error);
      return initialValue;
    }
  });

  const setValue = <T>(value: T) => {
    try {
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      setStoredValue(valueToStore);
      localStorage.setItem(key, JSON.stringify(valueToStore));
    } catch (error) {
      console.log(error);
    }
  };

  return [storedValue, setValue];
}
