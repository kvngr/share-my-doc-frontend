import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useAuthStore, selectAuthIsAuthenticated } from '@core';

export function useRedirect(pathToRedirect: string = '/dashboard') {
  const history = useHistory();
  const isAuthenticated = useAuthStore(selectAuthIsAuthenticated);

  useEffect(() => {
    if (isAuthenticated) {
      history.push(pathToRedirect);
    }
  }, [isAuthenticated]);
}
