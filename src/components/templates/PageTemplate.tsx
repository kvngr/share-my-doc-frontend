import React, { ReactNode } from 'react';
import { Main, RouteWrapper } from './assets/styles';
import styled from 'styled-components';

interface PageTemplateProps {
  header?: ReactNode;
  footer?: ReactNode;
  children: React.ReactNode;
}

const ChildrenWrapper = styled.div`
  margin: 60px 0;
`;

export const PageTemplate = ({ header, footer, children }: PageTemplateProps) => {
  return (
    <RouteWrapper>
      <Main>
        {header && <>{header}</>}
        <ChildrenWrapper>{children}</ChildrenWrapper>
        {footer && <>{footer}</>}
      </Main>
    </RouteWrapper>
  );
};
