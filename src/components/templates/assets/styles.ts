import styled from 'styled-components';

export const Main = styled.main`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
`;

export const RouteWrapper = styled.div`
  align-items: center;
  justify-content: center;
  min-width: 100%;
  min-height: 100%;
  display: flex;
  position: absolute;
  background-color: ${props => props.theme.backgroundColor};
`;
