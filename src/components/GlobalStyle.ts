import { createGlobalStyle } from 'styled-components';

export const animatedRoutes = {
  from: { opacity: 0 },
  enter: { opacity: 1 },
  leave: { opacity: 0 }
};

const GlobalStyle = createGlobalStyle`

*,
:before,
:after {
  box-sizing: border-box;
}


html,
body {
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
}

#app {
  overflow: hidden;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
}

#app > div > div {
  position: absolute;
  width: 100%;
}

#app > div {
  /* position: absolute; */
  width: 100%;
}

* {
    font-family: 'Roboto', sans-serif;
  }
`;

export default GlobalStyle;
