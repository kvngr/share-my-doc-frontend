import styled from 'styled-components';

export const FormWrapper = styled.div`
  height: 100%;
  display: flex;
`;

export const AbstractForm = styled.form<{ className?: string }>``;

export const Card = styled.div<{ width?: string }>`
  display: flex;
  width: ${({ width }) => width || '100%'};
  border: 0.8px solid rgba(0, 0, 0, 0.06);
  background: ${({ theme }) => theme.backgroundColor};
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
`;
