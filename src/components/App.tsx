import React, { useEffect } from 'react';
import GlobalStyle from './GlobalStyle';
import { useUIStore, useAuthStore } from '@core';
import { ThemeProvider } from 'styled-components';
import { Routes } from 'src/core/routes/Routes';
import { useAppIsLoaded } from 'src/core/stores/app.selectors';
import { lightTheme, darkTheme } from '@themes';

const App = () => {
  const { theme } = useUIStore();
  const { actions } = useAuthStore();
  const { actions: uiActions } = useUIStore();
  const isLoaded = useAppIsLoaded();

  useEffect(() => {
    const storedValue = localStorage.getItem('theme');
    actions.authenticate();

    switch (storedValue) {
      case 'light':
        uiActions.setTheme(lightTheme);
        break;
      case 'dark':
        uiActions.setTheme(darkTheme);
        break;
      default:
        uiActions.setTheme(lightTheme);
        break;
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      {isLoaded ? <Routes /> : 'Loading app...'}
      <GlobalStyle />
    </ThemeProvider>
  );
};

export default App;
