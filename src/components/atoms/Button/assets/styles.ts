import { colors } from '@themes';
import styled, { css } from 'styled-components';

interface ButtonProps {
  disabled?: boolean;
  busy?: boolean;
}

export const Label = styled.span`
  pointer-events: none;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  transition: opacity 0.2s cubic-bezier(0.4, 0, 0.2, 1);
  line-height: 1.5;
`;

export const Button = styled.button<ButtonProps>`
  display: inline-block;
  outline: none;
  line-height: normal;
  border: 0;
  border-radius: 100px;
  font-family: Roboto 'sans-serif';
  height: 45px;
  width: 100px;
  margin: auto;
  margin-top: 20px;
  font-size: 14px;
  position: relative;
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  transition: background 0.3s cubic-bezier(0.4, 0, 0.2, 1),
    background-position 0.3s cubic-bezier(0.4, 0, 0.2, 1),
    box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
  border-color: transparent;
  border: none;
  color: #ffff;
  background-color: ${colors.primary};
  background-repeat: no-repeat;
  background-image: linear-gradient(135deg, #3c6ac4, #4a90e2);
  background-position: 300px 0;
  ${props =>
    props.disabled &&
    css`
      cursor: initial;
      border: none;
      color: #ffff;
      background-color: #cccccc;
    `}

  ${props =>
    !props.disabled &&
    css`
      &:hover {
        background-position: 0 0;
        transition: background-color 0.3s cubic-bezier(0.4, 0, 0.2, 1),
          background-position 0.3s cubic-bezier(0.4, 0, 0.2, 1),
          box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
        box-shadow: 0 1px 2px rgba(64, 74, 86, 0.25);
      }
    `}
    ${props =>
      props.busy &&
      css`
        cursor: default;
        pointer-events: none;
        position: relative;

        > div {
          left: 50%;
          position: absolute;
          top: 50%;
          transform: translate(-50%, -50%);
        }
        span {
          opacity: 0;
        }
      `};
`;

export const SubmitButton = styled.input<ButtonProps>`
  display: inline-block;
  outline: none;
  line-height: normal;
  border: 0;
  border-radius: 100px;
  font-family: Roboto 'sans-serif';
  height: 45px;
  width: 100px;
  margin: auto;
  margin-top: 20px;
  font-size: 14px;
  position: relative;
  text-align: center;
  vertical-align: middle;
  cursor: pointer;
  transition: background 0.3s cubic-bezier(0.4, 0, 0.2, 1),
    background-position 0.3s cubic-bezier(0.4, 0, 0.2, 1),
    box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
  border-color: transparent;
  border: none;
  color: #ffff;
  background-color: ${colors.primary};
  background-repeat: no-repeat;
  background-image: linear-gradient(135deg, #3c6ac4, #4a90e2);
  background-position: 300px 0;
  ${props =>
    props.disabled &&
    css`
      cursor: initial;
      border: none;
      color: #ffff;
      background-color: #cccccc;
    `}

  ${props =>
    !props.disabled &&
    css`
      &:hover {
        background-position: 0 0;
        transition: background-color 0.3s cubic-bezier(0.4, 0, 0.2, 1),
          background-position 0.3s cubic-bezier(0.4, 0, 0.2, 1),
          box-shadow 0.3s cubic-bezier(0.4, 0, 0.2, 1);
        box-shadow: 0 1px 2px rgba(64, 74, 86, 0.25);
      }
    `}
    ${props =>
      props.busy &&
      css`
        cursor: default;
        pointer-events: none;
        position: relative;

        > div {
          left: 50%;
          position: absolute;
          top: 50%;
          transform: translate(-50%, -50%);
        }
        span {
          opacity: 0;
        }
      `};
`;

export const WrapperSubmitButton = styled.div`
  position: relative;
  overflow: hidden;
  display: inline-block;

  input[type='file'] {
    font-size: 100px;
    position: absolute;
    left: 0;
    top: 0;
    opacity: 0;
  }
`;
