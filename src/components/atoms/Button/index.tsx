import React from 'react';
import { Spinner } from '../Spinner';
import { Button as StyledButton, SubmitButton, Label, WrapperSubmitButton } from './assets/styles';

type ButtonType = 'submit' | 'button' | 'file';

interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

interface ButtonProps {
  children?: React.ReactNode;
  label?: string;
  value?: string;
  disabled?: boolean;
  secondary?: boolean;
  busy?: boolean;
  type?: ButtonType;
  className?: string;
  accept?: string;
  onClick?(event: React.MouseEvent<HTMLButtonElement>): void;
  onChange?(event: React.ChangeEvent<HTMLInputElement>): void;
}

export const Button = React.memo(
  ({
    children,
    accept,
    busy,
    disabled,
    label,
    type,
    value,
    onClick,
    onChange,
    secondary,
    className
  }: ButtonProps) => {
    switch (type) {
      case 'submit':
        return (
          <>
            <SubmitButton
              type="submit"
              className={className}
              value={label}
              busy={busy}
              disabled={disabled}
            />
            {busy && <Spinner small primary={secondary} position="absolute" />}
          </>
        );
      case 'file':
        return (
          <WrapperSubmitButton>
            <StyledButton onClick={onClick} busy={busy} disabled={disabled}>
              <Label>{children || label}</Label>
            </StyledButton>
            <SubmitButton
              type="file"
              className={className}
              value={value}
              accept={accept}
              onChange={onChange}
              busy={busy}
              disabled={disabled}
            />
            {busy && <Spinner small primary={secondary} position="absolute" />}
          </WrapperSubmitButton>
        );
      case 'button':
        return (
          <StyledButton onClick={onClick} className={className} busy={busy} disabled={disabled}>
            <Label>{children || label}</Label>
            {busy && <Spinner small primary={secondary} position="absolute" />}
          </StyledButton>
        );
      default:
        return (
          <StyledButton onClick={onClick} busy={busy} disabled={disabled}>
            <Label>{children || label}</Label>
            {busy && <Spinner small primary={secondary} position="absolute" />}
          </StyledButton>
        );
    }
  }
);
