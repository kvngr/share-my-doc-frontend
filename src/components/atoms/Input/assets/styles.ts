import styled, { css } from 'styled-components';
import { HighLight } from '../../../molecules/Field/assets/styles';
import { Label } from '../../../atoms/Label/assets/styles';
import { colors } from '@themes';
interface InputProps {
  autocomplete: string;
  error?: string | boolean;
  required?: boolean;
  type?: string;
  name: string;
  value: string;
  disabled?: boolean;
}

export const Input = styled.input<InputProps>`
  background: none;
  color: ${props => (props.disabled ? colors.dawnGrey : props.theme.color)};
  font-size: 18px;
  padding: 10px 10px 10px 5px;
  display: block;
  width: 320px;
  border: none;
  border-radius: 0;
  border-bottom: 1px solid ${colors.dawnGrey};

  ${props =>
    props.disabled &&
    css`
      border-bottom: 1px dashed ${colors.dawnGrey};
    `};

  &:focus {
    outline: none;
  }

  &:focus ~ .label-field {
    top: -14px;
    font-size: 12px;
    color: ${props =>
      (props.error && colors.error) ||
      (props.disabled && colors.dawnGrey) ||
      (!props.error && !props.disabled && colors.primary)};
  }

  &:not([value='']) ~ .label-field {
    top: -14px;
    font-size: 12px;
    color: ${props =>
      (props.error && colors.error) ||
      (props.disabled && colors.dawnGrey) ||
      (!props.error && !props.disabled && colors.primary)};
  }

  &:focus ~ span:before {
    width: 320px;
  }
`;
