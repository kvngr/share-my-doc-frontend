import styled from 'styled-components';

export interface Option {
  value: string;
  label: string;
}

export const SelectOption = styled.option`
  color: ${({ theme }) => theme.color};
`;
