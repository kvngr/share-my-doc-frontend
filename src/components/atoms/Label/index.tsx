import React from 'react';
import { Label } from './assets/styles';

interface LabelProps {
  label?: string;
  children?: React.ReactNode;
}

export default ({ label, children }: LabelProps) => <Label>{label || children}</Label>;
