import React from 'react';
import { IconProp, SizeProp } from '@fortawesome/fontawesome-svg-core';
import { Icon, IconWrapper } from './assets/styles';

export interface IconProps {
  icon: IconProp;
  className?: string;
  style?: object;
  color?: string;
  padding?: string;
  margin?: string;
  size?: SizeProp;
  to?: any;
  isActive?: boolean;
  onClick?: () => void;
}

export default ({
  className,
  onClick,
  style,
  icon,
  size,
  color,
  padding,
  margin,
  isActive
}: IconProps) => (
  <IconWrapper padding={padding} margin={margin}>
    <Icon
      style={style}
      size={size}
      color={color}
      className={className}
      isActive={isActive}
      icon={icon}
      onClick={onClick}
    />
  </IconWrapper>
);
