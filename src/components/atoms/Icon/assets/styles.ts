import styled, { css } from 'styled-components';
import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome';
import { colors } from '@themes';

interface IconProps extends FontAwesomeIconProps {
  color?: string;
  isActive?: boolean;
}

interface IconWrapperProps {
  padding?: string;
  margin?: string;
}

export const Icon = styled(FontAwesomeIcon)<IconProps>`
  color: ${props => props.color};
  ${({ isActive }) =>
    isActive &&
    css`
      color: ${colors.primary};
    `}
`;

export const IconWrapper = styled.div<IconWrapperProps>`
  padding: ${props => props.padding};
  margin: ${props => props.padding};
`;
