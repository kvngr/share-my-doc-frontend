import React from 'react';
import { SpinnerWrapper, BlockSpin, Spin } from './assets/styles';

type typePostion = 'absolute' | 'fixed' | 'static';

interface SpinnerProps {
  active?: boolean;
  position: typePostion;
  primary?: boolean;
  secondary?: boolean;
  small?: boolean;
}

export const Spinner = ({
  active = true,
  position,
  primary,
  secondary,
  small,
  ...props
}: SpinnerProps) => (
  <SpinnerWrapper
    {...props}
    active={active}
    primary={primary}
    secondary={secondary}
    small={small}
    position={position}
  >
    <BlockSpin>
      <Spin />
      <Spin />
      <Spin />
      <Spin />
    </BlockSpin>
  </SpinnerWrapper>
);

export default Spinner;
