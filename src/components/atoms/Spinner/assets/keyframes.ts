import styled, { keyframes } from 'styled-components';

export const moSpinnerMotion = keyframes`
  0% { opacity: 1; }
  100% { opacity: 0; }
`;

export const loader1 = keyframes`
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1);
  }
`;

export const loader2 = keyframes`
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(19px, 0);
  }
`;

export const loader3 = keyframes`
  0% {
    transform: scale(1);
  }
  100% {
    transform: scale(0);
  }
`;

export const loaderSmall1 = keyframes`
  0% {
    transform: scale(0);
  }
  100% {
    transform: scale(1);
  }
`;

export const loaderSmall2 = keyframes`
  0% {
    transform: translate(0, 0);
  }
  100% {
    transform: translate(14px, 0);
  }
`;

export const loaderSmall3 = keyframes`
  0% {
    transform: scale(1);
  }
  100% {
    transform: scale(0);
  }
`;
