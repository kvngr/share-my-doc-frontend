import styled, { css } from 'styled-components';
import { colors } from '@themes';
import * as keyframe from './keyframes';

interface SpinnerWrapperProps {
  position?: string;
  active?: boolean;
  small?: boolean;
  full?: boolean;
  secondary?: boolean;
  transparent?: boolean;
  dark?: boolean;
  light?: boolean;
  primary?: boolean;
}

export const Spin = styled.div`
  position: absolute;
  top: 2px;
  width: 11px;
  height: 11px;
  border-radius: 50%;
  background-image: linear-gradient(135deg, ${colors.lightGrey}, ${colors.white});
  animation-timing-function: cubic-bezier(0, 1, 1, 0);
  &:nth-child(1) {
    left: 6px;
    animation: ${keyframe.loader1} 0.6s infinite;
  }
  &:nth-child(2) {
    left: 6px;
    animation: ${keyframe.loader2} 0.6s infinite;
  }
  &:nth-child(3) {
    left: 26px;
    animation: ${keyframe.loader2} 0.6s infinite;
  }
  &:nth-child(4) {
    left: 45px;
    animation: ${keyframe.loader3} 0.6s infinite;
  }
`;

export const BlockSpin = styled.div`
  position: relative;
`;

export const SpinnerWrapper = styled.div<SpinnerWrapperProps>`
  display: none;
  height: 14px;
  width: 64px;
  z-index: 100;
  position: ${props => props.position};
  ${props =>
    props.position === 'absolute' &&
    css`
      left: 50%;
      top: 50%;
      transform: translate(-50%, -50%);
    `}
  ${props =>
    props.active &&
    css`
      display: block;
    `};
  ${props =>
    props.small &&
    css`
      height: 10px;
      width: 36px;
      ${BlockSpin} {
        ${Spin} {
          height: 6px;
          top: 2px;
          width: 6px;
          &:nth-child(1) {
            left: 0;
            animation: ${keyframe.loaderSmall1} 0.6s infinite;
          }
          &:nth-child(2) {
            left: 0;
            animation: ${keyframe.loaderSmall2} 0.6s infinite;
          }
          &:nth-child(3) {
            left: 15px;
            animation: ${keyframe.loaderSmall2} 0.6s infinite;
          }
          &:nth-child(4) {
            left: 30px;
            animation: ${keyframe.loaderSmall3} 0.6s infinite;
          }
        }
      }
    `};
  ${props =>
    props.primary &&
    css`
      ${BlockSpin} {
        ${Spin} {
          background-image: none;
          background-color: ${colors.white};
        }
      }
    `};
  ${props =>
    props.secondary &&
    css`
      ${BlockSpin} {
        ${Spin} {
          background-image: none;
          background-color: ${colors.primary};
        }
      }
    `};
`;
