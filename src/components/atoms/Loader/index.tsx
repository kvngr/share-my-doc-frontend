import React from 'react';
import { FingerprintSpinner } from 'react-epic-spinners';
import { WrapperLoader } from './assets/styles';

interface ILoaderProps {
  color?: string;
  bgColor?: string;
}

export const Loader = ({ color, bgColor }: ILoaderProps) => (
  <WrapperLoader bgColor={bgColor}>
    <FingerprintSpinner color={color} />
  </WrapperLoader>
);
