import styled, { css } from 'styled-components';

interface IWrapperLoaderProps {
  bgColor?: string;
}

export const WrapperLoader = styled.div<IWrapperLoaderProps>`
  position: absolute;
  margin: auto;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100px;
  height: 100px;
  background-color: ${props => props.theme.backgroundColor};
  border-radius: 3px;
`;
