import React, { ReactNode } from 'react';
import styled from 'styled-components';

type TitleType = 'h1' | 'h2' | 'h3' | 'h4';

interface TitleProps {
  type?: TitleType;
  label?: string;
  margin?: string | number;
  size?: string;
  children?: ReactNode;
}

interface HProps {
  margin?: string | number;
  size?: string;
}

const H1 = styled.h1<HProps>`
  font-size: ${({ size }) => size || 'initial'};
  margin: ${({ margin }) => margin || 'initial'};
`;
const H2 = styled.h2<HProps>`
  font-size: ${({ size }) => size || 'initial'};
  margin: ${({ margin }) => margin || 'initial'};
`;
const H3 = styled.h3<HProps>`
  font-size: ${({ size }) => size || 'initial'};
  margin: ${({ margin }) => margin || 'initial'};
`;
const H4 = styled.h4<HProps>`
  font-size: ${({ size }) => size || 'initial'};
  margin: ${({ margin }) => margin || 'initial'};
`;

export const Title = ({ type = 'h1', label, children, margin, size }: TitleProps) => {
  switch (type) {
    case 'h1':
      return (
        <H1 size={size} margin={margin}>
          {label || children}
        </H1>
      );
    case 'h2':
      return (
        <H2 size={size} margin={margin}>
          {label || children}
        </H2>
      );
    case 'h3':
      return (
        <H3 size={size} margin={margin}>
          {label || children}
        </H3>
      );
    case 'h4':
      return (
        <H4 size={size} margin={margin}>
          {label || children}
        </H4>
      );
    default:
      return (
        <H1 size={size} margin={margin}>
          {label || children}
        </H1>
      );
  }
};
