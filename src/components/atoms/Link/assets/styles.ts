import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import { colors } from '@themes';

interface LinkProps {
  isActive?: boolean;
}

export default styled(Link)<LinkProps>`
  text-decoration: none;
  margin: 10px;
  font-size: 14px;
  color: ${props => props.theme.color};
  ${({ isActive }) =>
    isActive &&
    css`
      color: ${colors.primary};
    `}
`;
