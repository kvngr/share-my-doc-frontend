import React, { ReactNode } from 'react';
import Link from './assets/styles';

export interface LinkProps {
  label?: string;
  to: string;
  children?: ReactNode;
  icon?: any;
  isActive?: boolean;
}

export default ({ label, children, isActive, to }: LinkProps) => (
  <Link isActive={isActive} to={to}>
    {label || children}
  </Link>
);
