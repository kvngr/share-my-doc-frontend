import React, { ReactNode } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { NavDesktop, NavMobile } from './assets/styles';
import Link, { LinkProps } from '../../atoms/Link';
import { IconProps } from '../../atoms/Icon';
import Icon from '../../atoms/Icon';
import { userIcon } from '../../../utils/icons';
import { colors } from '@themes';

interface NaVprops extends RouteComponentProps<any> {
  links?: LinkProps[];
  icons?: IconProps[];
  isDesktop: boolean;
}

export default ({ links = [], icons = [], isDesktop, match, history, location }: NaVprops) => {
  return (
    <>
      {isDesktop && (
        <NavDesktop>
          {links.map(link => (
            <Link
              key={link.label}
              isActive={link.to === match.path}
              to={link.to}
              label={link.label}
            />
          ))}
        </NavDesktop>
      )}
      {!isDesktop && (
        <NavMobile>
          {icons.map((element, index) => (
            <Link to={element.to} key={index}>
              <Icon size="2x" isActive={element.to === match.path} icon={element.icon} />
            </Link>
          ))}
        </NavMobile>
      )}
    </>
  );
};
