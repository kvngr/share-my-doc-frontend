import styled, { css } from 'styled-components';

export const NavDesktop = styled.nav`
  display: flex;
  flex: 1;
`;

export const NavMobile = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100%;
`;
