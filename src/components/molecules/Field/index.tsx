import React, { forwardRef, ReactNode } from 'react';
import { Input } from '../../atoms/Input';
import Label from '../../atoms/Label';
import { Error, Wrapper, HighLight, Bar } from './assets/styles';
import { Ref } from 'react-hook-form/dist/types';

interface FieldProps {
  disabled?: boolean;
  value?: string;
  label?: string;
  name: string;
  type?: string;
  width?: string;
  height?: string;
  placeholder?: string;
  error?: string | boolean;
  className?: string;
  children?: ReactNode;
  min?: number;
  max?: number;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (event: React.ChangeEvent<HTMLInputElement>) => void | undefined;
  onFocus?: (event: React.ChangeEvent<HTMLInputElement>) => void | undefined;
  onMouseOver?: (event: React.SyntheticEvent) => void | undefined;
  onMouseOut?: (event: React.SyntheticEvent) => void | undefined;
}

export const Field = forwardRef(
  (
    {
      children,
      label,
      error,
      placeholder,
      value,
      disabled,
      type,
      name,
      onChange,
      onBlur,
      onFocus,
      onMouseOver,
      onMouseOut,
      className,
      width,
      height,
      min,
      max
    }: FieldProps,
    ref: Ref
  ) => {
    return (
      <>
        <Wrapper className={className}>
          <Input
            ref={ref}
            disabled={disabled}
            placeholder={placeholder}
            type={type}
            value={value}
            name={name}
            onChange={onChange}
            onBlur={onBlur}
            onFocus={onFocus}
            onMouseOver={onMouseOver}
            onMouseOut={onMouseOut}
            error={!!error}
            width={width}
            height={height}
            min={min}
            max={max}
          />
          <Label>{label}</Label>
          <HighLight />
          <Bar error={!!error} />
          <Error error={!!error}>{error}</Error>
        </Wrapper>
        {children}
      </>
    );
  }
);
