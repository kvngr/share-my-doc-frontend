const { headShake } = require('react-animations');
import { colors } from '@themes';
import styled, { css, keyframes } from 'styled-components';

const ShakeAnimation = keyframes`${headShake}`;

interface FieldProps {
  error: string | boolean;
}

export const Error = styled.span<FieldProps>`
  z-index: -1;
  opacity: 0;
  font-size: 11px;
  height: 20px;
  margin: 20px 0;
  transform: translateY(-30px);
  ${props =>
    props.error &&
    css`
      color: ${colors.error};
      z-index: 1;
      opacity: 1;
      transform: translateY(0px);
    `};
  transition: opacity 0.2s cubic-bezier(0.4, 0, 0.2, 1), transform 0.2s cubic-bezier(0.4, 0, 0.2, 1);
`;

export const Wrapper = styled.div`
  position: relative;
  margin: 5px 45px 0 45px;
  height: 70px;
`;

export const HighLight = styled.span``;

export const Bar = styled.span<FieldProps>`
  position: relative;
  display: block;
  width: 320px;

  &:before {
    content: '';
    height: 2px;
    width: 0;
    bottom: 0px;
    position: absolute;
    background: ${colors.primary};
    transition: 300ms ease all;
    left: 0%;
  }
  ${props =>
    props.error &&
    css`
      &:before {
        background: ${colors.error};
        width: 100%;
      }
    `}
`;
