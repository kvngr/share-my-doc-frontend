import React, { forwardRef } from 'react';
import styled from 'styled-components';
import { Option, SelectOption } from 'src/components/atoms/Option';
import { Ref } from 'react-hook-form/dist/types';

interface SelectProps {
  options: Option[];
  name?: string;
  value?: string | undefined;
  className?: string;
  onChange?: (event: React.ChangeEvent<HTMLSelectElement>) => void;
}

const StyledSelect = styled.select`
  outline: none;
  /* border: none; */
  background: transparent;
  color: ${({ theme }) => theme.color};
  font-size: 14px;
`;

export const Select = forwardRef(
  ({ options, className, onChange, name, value }: SelectProps, ref: Ref) => (
    <StyledSelect name={name} className={className} ref={ref} onChange={onChange} value={value}>
      {options.map((option, index) => (
        <SelectOption key={index} value={option.value}>
          {option.label}
        </SelectOption>
      ))}
      ;
    </StyledSelect>
  )
);
