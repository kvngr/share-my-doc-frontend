import styled, { css } from 'styled-components';

export const FooterLayout = styled.div`
  background-color: ${props => props.theme.backgroundColor};
  padding: 30px;
  text-align: center;
  font-size: 35px;
  color: ${props => props.theme.color};
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 15px 10px 25px rgba(0, 0, 0, 0.09);
`;
