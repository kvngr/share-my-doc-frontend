import React from 'react';
import { FooterLayout } from './assets/styles';

export const Footer = ({ children }: { children: React.ReactNode }) => (
  <FooterLayout>{children}</FooterLayout>
);
