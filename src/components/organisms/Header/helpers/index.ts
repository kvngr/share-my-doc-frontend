import {
  userIcon,
  homeIcon,
  signInIcon,
  logoutIcon,
  dashboardIcon,
  adminIcon
} from '../../../../utils/icons';

export const linkAdmin = {
  label: 'Administration',
  to: '/administration'
};

export const navigationLink = [
  {
    label: 'Profil',
    to: '/profile'
  },
  {
    label: 'Dashboard',
    to: '/dashboard'
  }
];

export const userNavigationIcon = [
  {
    icon: userIcon,
    to: '/profile'
  },
  {
    icon: dashboardIcon,
    to: '/dashboard'
  },
  {
    icon: logoutIcon,
    to: '/login'
  }
];

export const selectOptions = [
  {
    label: 'Clair',
    value: 'light'
  },
  {
    label: 'Sombre',
    value: 'dark'
  }
];
