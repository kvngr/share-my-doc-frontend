import React, { ReactNode } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { DesktopLayoutMenu, MobileLayoutMenu } from './assets/styles';
import Nav from '../../molecules/Nav';
import { navigationLink, linkAdmin, userNavigationIcon, selectOptions } from './helpers';

import { useWindowSize } from '../../../core/hooks/useWindowSize';
import { lightTheme, darkTheme } from '@themes';

import { useUIStore, useAuthStore } from '@core';
import { UserRole } from 'src/core/types/user';
import { Select } from 'src/components/molecules/Select';
import Icon from '../../atoms/Icon';
import Link from '../../atoms/Link';
import { WrapperLogout } from './assets/styles';
import { logoutIcon } from 'src/utils/icons';
// import { UserRole } from 'src/core/stores/auth/auth.actions';

const WIDTH = 992;

const DESKTOP_WINDOW_SIZE = WIDTH;

interface HeaderProps extends RouteComponentProps<any> {
  children?: ReactNode;
  userRole: UserRole;
}

export const Header = ({ children, userRole, match, history, location }: HeaderProps) => {
  if (userRole === UserRole.ADMIN) {
    navigationLink.push(linkAdmin);
  }

  const size = useWindowSize();

  const { actions } = useUIStore();
  const { actions: authActions } = useAuthStore();

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    switch (event.target.value) {
      case 'light':
        actions.setTheme(lightTheme);
        break;
      case 'dark':
        actions.setTheme(darkTheme);
        break;
      default:
        actions.setTheme(lightTheme);
    }
  };

  return (
    <>
      {size.width && size.width >= DESKTOP_WINDOW_SIZE && (
        <DesktopLayoutMenu>
          <Nav
            match={match}
            history={history}
            location={location}
            isDesktop={size.width >= DESKTOP_WINDOW_SIZE}
            links={navigationLink}
          />
          {children}
          <Select
            options={selectOptions}
            onChange={handleChange}
            value={localStorage.getItem('theme') || undefined}
          />
          <WrapperLogout onClick={authActions.logout}>
            <Link to="/login">
              <Icon size="2x" icon={logoutIcon} />
            </Link>
          </WrapperLogout>
        </DesktopLayoutMenu>
      )}
      {size.width && size.width < DESKTOP_WINDOW_SIZE && (
        <MobileLayoutMenu>
          <Nav
            match={match}
            history={history}
            location={location}
            isDesktop={size.width >= DESKTOP_WINDOW_SIZE}
            icons={userNavigationIcon}
          />
        </MobileLayoutMenu>
      )}
    </>
  );
};
