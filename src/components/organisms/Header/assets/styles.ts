import styled from 'styled-components';

export const DesktopLayoutMenu = styled.div`
  z-index: 1000;
  width: 100%;
  position: fixed;
  top: 0;
  display: flex;
  align-items: center;
  background-color: ${props => props.theme.backgroundColor};
  padding: 10px;
  text-align: center;
  font-size: 35px;
  color: ${props => props.theme.color};
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
`;

export const MobileLayoutMenu = styled.div`
  display: flex;
  width: 100%;
  position: fixed;
  background: ${props => props.theme.backgroundColor};
  bottom: 0;
  overflow: hidden;
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
  z-index: 100;
`;

export const WrapperLogout = styled.div`
  display: flex;
  cursor: pointer;
  margin-left: 15px;
`;
