import React, { forwardRef, useRef, useEffect, Ref, memo } from 'react';

interface IndeterminateCheckboxProps<T> {
  indeterminate?: T;
}

export const IndeterminateCheckbox = memo(
  forwardRef(
    <T extends unknown>({ indeterminate, ...rest }: IndeterminateCheckboxProps<T>, ref: Ref<T>) => {
      const defaultRef = useRef<HTMLInputElement>(null);
      const resolvedRef = ref || defaultRef;

      useEffect(() => {
        resolvedRef.current.indeterminate = indeterminate;
      }, [resolvedRef, indeterminate]);

      return (
        <>
          <input type="checkbox" ref={resolvedRef} {...rest} />
        </>
      );
    }
  )
);
