export const showingOptions = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50];

// const defineUserHeader = (label: string): string | null => {
//   let formattedLabel: string | null = null;
//   switch (label) {
//     case 'Id':
//       formattedLabel = 'ID';
//       break;
//     case 'firstName':
//       formattedLabel = 'Prénom';
//       break;
//     case 'lastName':
//       formattedLabel = 'Nom';
//       break;
//     case 'email':
//       formattedLabel = 'Email';
//       break;
//     case 'role':
//       formattedLabel = 'Rôle';
//       break;
//     default:
//       formattedLabel = null;
//       break;
//   }
//   return formattedLabel;
// };

export const renderColumns = (data: any[] = []) => {
  if (!Array.isArray(data)) {
    throw new Error('🥶 Invalid iterable array');
  }

  if (!data.length) return data;

  const objectWithMaxKeys = data.reduce((acc, current) => {
    if (Object.keys(acc).length < Object.keys(current).length) {
      return current;
    }
    return acc;
  }, data[0]);

  return Object.keys(objectWithMaxKeys)
    .map(accessor => ({
      Header: accessor.replace(/([A-Z])/g, '$1').replace(/^./, str => str.toUpperCase()),
      accessor,
      id: accessor,
      align: 'right'
    }))
    .filter(column => column.Header !== '__typename');
};

export const getListId = (data: any[] = [], options?: { oneId: boolean }) => {
  const id = data.map(element => element.values.id);

  switch (options && options.oneId) {
    case true:
      return id.pop();
    case false:
      return id;
    default:
      return id;
  }
};
