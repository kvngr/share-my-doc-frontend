import React, { memo, useEffect } from 'react';
import useForm from 'react-hook-form';
import { DomainFormData, FormType } from '../../types';
import { useDomainStore, useTableStore } from '@core';
import { Field } from 'src/components/molecules/Field';
import { Button } from 'src/components/atoms/Button';
import Validator from 'src/utils/validator/Validator';
import { errorsList } from 'src/utils/errors';

interface FormDomainProps {
  formType: FormType;
}

const validator = new Validator();

export const FormDomain = memo(({ formType }: FormDomainProps) => {
  const {
    register,
    unregister,
    errors,
    watch,
    setValue,
    handleSubmit,
    setError,
    clearError
  } = useForm<DomainFormData>({ mode: 'onChange' });

  const { rowId } = useTableStore();
  const id = rowId;
  const { actions } = useDomainStore();

  useEffect(() => {
    return () => {
      unregister('name');
      unregister('description');
    };
  }, [register]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setValue(event.target.name as 'name' | 'description', event.target.value);

  const onCreate = handleSubmit(({ name, description }) => {
    actions.createDomain(name, description);
  });

  const onUpdate = handleSubmit(({ name, description }) => {
    actions.updateDomain(id, name, description);
  });

  const nameValue = watch('name');
  const descriptionValue = watch('description');

  return (
    <>
      {formType === 'CREATE' && (
        <form className="form-table-modal" onSubmit={onCreate}>
          <Field
            name="name"
            ref={register({
              validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
            })}
            type="text"
            label="Titre"
            value={nameValue}
            onBlur={() => setError('name', 'invalidDomainName', errorsList.generic.emptyField)}
            onFocus={() => clearError('name')}
            onChange={handleChange}
            error={errors.name && errors.name.message}
          />
          <Field
            name="description"
            ref={register({
              validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
            })}
            type="text"
            label="Description"
            value={descriptionValue}
            onBlur={() =>
              setError('description', 'invalidDescription', errorsList.generic.emptyField)
            }
            onFocus={() => clearError('description')}
            onChange={handleChange}
            error={errors.description && errors.description.message}
          />
          <Button className="button-form-table-modal" type="submit" label="Créer un domaine" />
        </form>
      )}
      {formType === 'UPDATE' && (
        <form className="form-table-modal" onSubmit={onUpdate}>
          <Field disabled name="id" label="ID" value={rowId} />
          <Field
            name="name"
            ref={register({
              validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
            })}
            type="text"
            label="Titre"
            value={nameValue}
            onBlur={() => setError('name', 'invalidDomainName', errorsList.generic.emptyField)}
            onFocus={() => clearError('name')}
            onChange={handleChange}
            error={errors.name && errors.name.message}
          />
          <Field
            name="description"
            ref={register({
              validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
            })}
            type="text"
            label="Description"
            value={descriptionValue}
            onBlur={() =>
              setError('description', 'invalidDescription', errorsList.generic.emptyField)
            }
            onFocus={() => clearError('description')}
            onChange={handleChange}
            error={errors.description && errors.description.message}
          />
          <Button className="button-form-table-modal" type="submit" label="Modifier un domaine" />
        </form>
      )}
    </>
  );
});
