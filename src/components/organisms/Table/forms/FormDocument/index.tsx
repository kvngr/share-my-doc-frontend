import React, { memo, useEffect, useState } from 'react';
import styled, { css } from 'styled-components';
import useForm from 'react-hook-form';
import Validator from 'src/utils/validator/Validator';
import { Field } from 'src/components/molecules/Field';
import { Button } from 'src/components/atoms/Button';
import { errorsList } from 'src/utils/errors';
import { DocumentFormData, FormType } from '../../types';
import { useDocumentReducer, Rule } from './reducer';
import {
  incrementVersion,
  decrementVersion,
  incrementRevision,
  decrementRevision,
  addRule,
  removeRule
} from './actions';
import { colors } from '@themes';
import Icon from 'src/components/atoms/Icon';
import { trashIcon, addIcon } from 'src/utils/icons';
import { useDocumentStore } from '@core';

const validator = new Validator();

interface FormDocumentProps {
  formType: FormType;
}

interface FileDateState {
  file: File;
  base64: string | ArrayBuffer | null;
}

const CounterWrapper = styled.div<{ disabled?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 320px;
  margin: 0 45px 20px 45px;
  color: ${props => props.theme.color};

  .btn-wrapper {
    button {
      background: none;
      outline: none;
      border: none;
      font-size: 28px;
      color: ${props => props.theme.color};
      ${props =>
        props.disabled &&
        css`
          color: ${colors.grey};
        `}
    }
  }
`;

const RuleBlock = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  .rule-row {
    width: 320px;
    margin: 0 45px 20px 45px;
  }

  .btn-row-rule {
    background: none;
    border: none;
    outline: none;
  }

  .disabled-btn-row-rule {
    cursor: not-allowed;
  }
`;

const Rule = styled.div`
  flex: 1;
`;

const FieldWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 320px;

  button {
    margin: 0 0 20px;
  }

  .field-rule {
    margin: 0;
  }
`;

const RuleWrapper = styled.div`
  display: flex;
  width: 100%;
  margin: auto;
  align-items: center;
`;

const BtnWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: 0 0 20px 0;
`;

const Label = styled.span`
  color: ${props => props.theme.color};
`;

export const FormDocument = memo(({ formType }: FormDocumentProps) => {
  const {
    register,
    unregister,
    setError,
    clearError,
    handleSubmit,
    errors,
    watch,
    getValues,
    formState,
    setValue
  } = useForm<DocumentFormData>({
    mode: 'onChange'
  });

  useEffect(() => {
    return () => {
      unregister('title');
      unregister('domainId');
      unregister('rule');
    };
  }, [register]);

  const { actions } = useDocumentStore();
  const [state, dispatch] = useDocumentReducer();
  const { revision, version, ruleIds } = state;
  const [fileData, setFileData] = useState<FileDateState | null>(null);

  const onCreate = handleSubmit(({ title, rule, domainId }) => {
    const rules: string[] = [];

    ruleIds.map(element => {
      rules.push(element.value);
    });

    const document = {
      title,
      ruleIds: [...rules],
      domainId,
      revision,
      version
    };

    actions.createDocument({ document });
  });

  const titleValue = watch('title');
  const domainIdValue = watch('domainId');
  const ruleValue = watch('rule');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setValue(event.target.name as 'title' | 'domainId' | 'rule', event.target.value);

  const handleAddRule = (rule: string) => {
    const isExist = ruleIds.some(element => element.value === rule);
    if (rule && !isExist) {
      dispatch(addRule(rule));
    }
  };

  const handleChangeFile = (event: any) => {
    const target = event.target as HTMLInputElement;
    const document: File = (target.files as FileList)[0];
    actions.uploadDocument(document);
  };

  const isRequired = ruleIds.length === 0 && !ruleValue;

  return (
    <>
      {formType === 'CREATE' && fileData !== null &&
        <div>
          HELLO
        </div>
      }
      {formType === 'CREATE' && fileData === null && (
        <form onSubmit={onCreate} className="form-table-modal">
          <Field
            name="title"
            ref={register({
              validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
            })}
            type="text"
            label="Title"
            value={titleValue}
            onBlur={() => setError('title', 'invalidTitle', errorsList.generic.emptyField)}
            onFocus={() => clearError('title')}
            onChange={handleChange}
            error={errors.title && errors.title.message}
          />
          <Field
            name="domainId"
            ref={register({
              validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
            })}
            type="text"
            label="ID du domaine"
            value={domainIdValue}
            onBlur={() => setError('domainId', 'invalidDomainId', errorsList.generic.emptyField)}
            onFocus={() => clearError('domainId')}
            onChange={handleChange}
            error={errors.domainId && errors.domainId.message}
          />
          <RuleBlock>
            <FieldWrapper>
              <Field
                name="rule"
                className="field-rule"
                ref={register({ required: isRequired })}
                type="text"
                placeholder="Ajouter une nouvelle Règle"
                value={ruleValue}
                onBlur={() => setError('rule', 'invalidRule', errorsList.generic.emptyField)}
                onFocus={() => clearError('rule')}
                onChange={handleChange}
                error={errors.rule && errors.rule.message}
              />
              <button
                className={`btn-row-rule ${ruleValue === '' && 'disabled-btn-row-rule'}`}
                type="button"
                onClick={() => handleAddRule(ruleValue)}
                disabled={ruleValue === ''}
              >
                <Icon
                  size="2x"
                  color={ruleValue === '' ? colors.grey : colors.blue}
                  icon={addIcon}
                />
              </button>
            </FieldWrapper>
            <div className="rule-row">
              {ruleIds.map((element: Rule) => (
                <RuleWrapper key={element.id}>
                  <Rule>{element.value}</Rule>
                  <button
                    className="btn-row-rule"
                    type="button"
                    onClick={() => dispatch(removeRule(element.id))}
                  >
                    <Icon size="2x" color={colors.red} icon={trashIcon} />
                  </button>
                </RuleWrapper>
              ))}
            </div>
          </RuleBlock>
          <CounterWrapper>
            <span>Numéro de version : {state.version}</span>
            <div className="btn-wrapper">
              <button
                className="btn-row-rule"
                type="button"
                onClick={() => dispatch(incrementVersion(1))}
              >
                +
              </button>
              <button
                className="btn-row-rule"
                type="button"
                onClick={() => dispatch(decrementVersion(1))}
                disabled={state.version === 1}
              >
                -
              </button>
            </div>
          </CounterWrapper>
          <CounterWrapper>
            <span>Numéro de révision : {state.revision}</span>
            <div className="btn-wrapper">
              <button
                className="btn-row-rule"
                type="button"
                onClick={() => dispatch(incrementRevision(1))}
              >
                +
              </button>
              <button
                type="button"
                onClick={() => dispatch(decrementRevision(1))}
                disabled={state.revision === 0}
              >
                -
              </button>
            </div>
          </CounterWrapper>
          <BtnWrapper>
            <Button
              disabled={!formState.isValid}
              className="button-form-table-modal"
              type="submit"
              label="Créer un document"
            />
            <Label>Ou</Label>
            <Button
              type="file"
              onChange={handleChangeFile}
              label="Téléverser"
              // value="Téléverser un document"
              accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            />
          </BtnWrapper>
        </form>
      )}
    </>
  );
});
