export enum ActionType {
  IncrementVersion = 'INCREMENT_VERSION',
  DecrementVersion = 'DECREMENT_VERSION',
  IncrementRevision = 'INCREMENT_REVISION',
  DecrementRevision = 'DECREMENT_REVISION',
  AddRule = 'ADD_RULE',
  RemoveRule = 'REMOVE_RULE'
}

export function incrementVersion(count: number) {
  return <const>{
    type: ActionType.IncrementVersion,
    count
  };
}

export function decrementVersion(count: number) {
  return <const>{
    type: ActionType.DecrementVersion,
    count
  };
}

export function incrementRevision(count: number) {
  return <const>{
    type: ActionType.IncrementRevision,
    count
  };
}

export function decrementRevision(count: number) {
  return <const>{
    type: ActionType.DecrementRevision,
    count
  };
}

export function addRule(rule: string) {
  return <const>{
    type: ActionType.AddRule,
    rule
  };
}

export function removeRule(id: number) {
  return <const>{
    type: ActionType.RemoveRule,
    id
  };
}

export type Actions = ReturnType<
  | typeof incrementVersion
  | typeof decrementVersion
  | typeof incrementRevision
  | typeof decrementRevision
  | typeof addRule
  | typeof removeRule
>;
