import { useReducer } from 'react';
import { Actions, ActionType } from './actions';

export interface Rule {
  id: number;
  value: string;
}

interface State {
  version: number;
  revision: number;
  ruleIds: Rule[];
}

let nextId: number = 0;

const initialState = { revision: 0, version: 1, ruleIds: [] };
// type State = typeof initialState;

function documentReducer(state: State, action: Actions): State {
  switch (action.type) {
    // update rersion
    case ActionType.IncrementVersion:
      return { ...state, version: state.version + 1 };
    case ActionType.DecrementVersion:
      return { ...state, version: state.version - 1 };
    // update revision
    case ActionType.IncrementRevision:
      return { ...state, revision: state.revision + 1 };
    case ActionType.DecrementRevision:
      return { ...state, revision: state.revision - 1 };
    // Add rule
    case ActionType.AddRule:
      return {
        ...state,
        ruleIds: [...state.ruleIds, { id: nextId++, value: action.rule }]
      };
    // Remove rule
    case ActionType.RemoveRule:
      return { ...state, ruleIds: [...state.ruleIds.filter(item => item.id !== action.id)] };
    // by default return state
    default:
      return state;
  }
}

export function useDocumentReducer(state = initialState) {
  return useReducer(documentReducer, state);
}
