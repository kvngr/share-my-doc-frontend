import React, { memo, useEffect } from 'react';
import { Select } from 'src/components/molecules/Select';
import useForm from 'react-hook-form';
import Validator from 'src/utils/validator/Validator';
import { Field } from 'src/components/molecules/Field';
import { Button } from 'src/components/atoms/Button';
import { errorsList } from 'src/utils/errors';
import { UserFormData, FormType } from '../../types';
import { useUserStore, useTableStore } from '@core';

const validator = new Validator();

interface FormUserProps {
  formType: FormType;
}

export const FormUser = memo(({ formType }: FormUserProps) => {
  const {
    register,
    unregister,
    setError,
    clearError,
    handleSubmit,
    errors,
    watch,
    setValue
  } = useForm<UserFormData>({
    mode: 'onChange'
  });

  const { actions } = useUserStore();
  const { rowId } = useTableStore();

  const onCreate = handleSubmit(
    ({ role, firstName, lastName, email, password, domainId, allowedDomainIds }) => {
      const regex = /\s*(\s|$)\s*/;
      const tmp = ' ';

      const domainList = allowedDomainIds.split(regex).filter(item => item !== tmp);

      const user = {
        role,
        firstName,
        lastName,
        email,
        password,
        domainId,
        allowedDomainIds: domainList
      };

      actions.createUser({ user });
    }
  );

  const onUpdate = handleSubmit(({ firstName, lastName }) => {
    const id = rowId;
    actions.updateUser(id, firstName, lastName);
  });

  // const roleValue = watch('role');
  const firstNameValue = watch('firstName');
  const lastNameValue = watch('lastName');
  const emailValue = watch('email');
  const passwordValue = watch('password');
  const domainIdValue = watch('domainId');
  const allowedDomainsIdsValue = watch('allowedDomainIds');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setValue(
      event.target.name as
        | 'email'
        | 'password'
        | 'allowedDomainIds'
        | 'domainId'
        | 'firstName'
        | 'lastName',
      event.target.value
    );

  useEffect(() => {
    return () => {
      unregister('role');
      unregister('firstName');
      unregister('lastName');
      unregister('email');
      unregister('password');
      unregister('domainId');
      unregister('allowedDomainIds');
    };
  }, [register]);

  const options = [
    { label: 'USER', value: 'USER' },
    { label: 'ADMIN', value: 'ADMIN' }
  ];

  return (
    <>
      {formType === 'CREATE' && (
        <form onSubmit={onCreate} className="form-table-modal">
          <Select
            name="role"
            className="select-form-table-modal"
            ref={register}
            options={options}
          />
          <Field
            name="firstName"
            ref={register({
              validate: value => validator.nameIsValid(value) || errorsList.input.firstName
            })}
            type="text"
            label="Prénom"
            value={firstNameValue}
            onBlur={() => setError('firstName', 'invalidFirstName', errorsList.input.firstName)}
            onFocus={() => clearError('firstName')}
            onChange={handleChange}
            error={errors.firstName && errors.firstName.message}
          />
          <Field
            name="lastName"
            ref={register({
              validate: value => validator.nameIsValid(value) || errorsList.input.lastName
            })}
            type="text"
            label="Nom"
            value={lastNameValue}
            onBlur={() => setError('lastName', 'invalidLastName', errorsList.input.lastName)}
            onFocus={() => clearError('lastName')}
            onChange={handleChange}
            error={errors.lastName && errors.lastName.message}
          />
          <Field
            name="email"
            value={emailValue}
            ref={register({
              validate: value => validator.emailIsValid(value) || errorsList.input.firstName
            })}
            type="email"
            label="Adresse email"
            onBlur={() => setError('email', 'invalidEmail', errorsList.input.email)}
            onFocus={() => clearError('email')}
            onChange={handleChange}
            error={errors.email && errors.email.message}
          />
          <Field
            name="password"
            ref={register({
              validate: value => validator.passwordIsvalid(value) || errorsList.generic.emptyField
            })}
            value={passwordValue}
            type="password"
            label="Mot de passe"
            onBlur={() => setError('password', 'invalidPassword', errorsList.generic.emptyField)}
            onFocus={() => clearError('password')}
            onChange={handleChange}
            error={errors.password && errors.password.message}
          />
          <Field
            name="domainId"
            ref={register({
              validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
            })}
            type="text"
            label="Domaine principal"
            value={domainIdValue}
            onBlur={() => setError('domainId', 'invalidDomainId', errorsList.generic.emptyField)}
            onFocus={() => clearError('domainId')}
            onChange={handleChange}
            error={errors.domainId && errors.domainId.message}
          />
          <Field
            name="allowedDomainIds"
            ref={register}
            type="text"
            label="Domaines autorisés"
            onBlur={() =>
              setError(
                'allowedDomainIds',
                'invalidAllowedDomainsIds',
                errorsList.generic.emptyField
              )
            }
            onFocus={() => clearError('allowedDomainIds')}
            onChange={handleChange}
            error={errors.allowedDomainIds && errors.allowedDomainIds.message}
            value={allowedDomainsIdsValue}
          />
          <Button className="button-form-table-modal" type="submit" label="Créer un utilisateur" />
        </form>
      )}
      {formType === 'UPDATE' && (
        <form onSubmit={onUpdate} className="form-table-modal">
          <Field disabled name="id" width="400px" label="ID" value={rowId} />
          <Field
            name="firstName"
            width="400px"
            ref={register({
              validate: value => validator.nameIsValid(value) || errorsList.input.firstName
            })}
            type="text"
            label="Prénom"
            value={firstNameValue}
            onBlur={() => setError('firstName', 'invalidFirstName', errorsList.input.firstName)}
            onFocus={() => clearError('firstName')}
            onChange={handleChange}
            error={errors.firstName && errors.firstName.message}
          />
          <Field
            name="lastName"
            width="400px"
            ref={register({
              validate: value => validator.nameIsValid(value) || errorsList.input.lastName
            })}
            type="text"
            label="Nom"
            value={lastNameValue}
            onBlur={() => setError('lastName', 'invalidLastName', errorsList.input.lastName)}
            onFocus={() => clearError('lastName')}
            onChange={handleChange}
            error={errors.lastName && errors.lastName.message}
          />
          <Button
            className="button-form-table-modal"
            type="submit"
            label="Modifier un utilisateur"
          />
        </form>
      )}
    </>
  );
});
