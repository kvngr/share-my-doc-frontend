import React, { memo, useEffect } from 'react';
import useForm from 'react-hook-form';
import { FormType, RulesFormData } from '../../types';
import Validator from 'src/utils/validator/Validator';
import { useRuleStore } from '@core';

interface FormRulesProps {
  formType: FormType;
}

const validator = new Validator();

export const FormRues = memo(({ formType }: FormRulesProps) => {
  const { register, unregister, errors, setError, clearError, handleSubmit } = useForm<
    RulesFormData
  >({ mode: 'onChange' });

  const { actions } = useRuleStore();

  const onSubmit = handleSubmit(({ title, content, version, revision, domainId, image }) => {
    const rule = {
      title,
      content,
      version,
      revision,
      domainId,
      image
    };
  });

  useEffect(() => {
    return () => {
      unregister('title');
      unregister('content');
      unregister('version');
      unregister('title');
    };
  }, [register]);

  return <form onSubmit={onSubmit}></form>;
});
