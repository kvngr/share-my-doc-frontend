import React, { memo, useEffect } from 'react';
import styled from 'styled-components';
import { AbstractModal as Modal } from 'src/components/organisms/Modal';
import { Button } from 'src/components/atoms/Button';
import { Field } from 'src/components/molecules/Field';
import { defineModalSize, defineLabel } from '../Modal/helpers';
import { colors } from '@themes';
import { Title } from 'src/components/atoms/Title';

import { FormUser } from './forms/FormUser';

import './assets/styles.scss';
import { FormType } from './types';
import { FormDomain } from './forms/FormDomain';
import { FormDocument } from './forms/FormDocument';

interface TableModal {
  modalType: string | null;
  label?: string;
  active: boolean;
  data?: any;
  onClose: () => void;
  onDelete: () => void;
}

const ContentTableModal = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  flex: 1;
`;

const BtnGroup = styled.div`
  display: flex;

  > button {
    margin: 20px;
    font-size: 18px;
    font-weight: 400;
    background: none;
    background-image: none;
    border: none;
    color: ${props => props.theme.color};
    transition: none;

    &:hover {
      color: ${colors.primary};
      background-image: none;
      box-shadow: none;
    }
  }
`;

const Infos = styled.div<{ weight?: number }>`
  color: ${props => props.theme.color};
  text-align: center;
  padding: 20px;
  font-size: 17px;
  font-weight: ${props => props.weight};
`;

const defineContentModal = (label: string, formType: FormType) => {
  switch (label) {
    case 'Utilisateurs':
      return <FormUser formType={formType} />;
    case 'Domaines':
      return <FormDomain formType={formType} />;
    case 'Documents':
      return <FormDocument formType={formType} />;
    // case 'Régles':
    //   return <FormRule formType={formType} />;
    default:
      break;
  }
};

export const TableModal = memo(
  ({ data, modalType, active, onClose, label, onDelete }: TableModal) => {
    const size = defineModalSize(modalType);

    const { width, height } = size;

    return (
      <>
        {
          <Modal
            width={width}
            height={height}
            onClose={onClose}
            closeOnEscape={onClose}
            closeOnClickOverlay={onClose}
            active={active}
            persist={modalType === 'DELETE'}
          >
            {modalType === 'CREATE' && defineContentModal(label!, 'CREATE')}
            {modalType === 'UPDATE' && defineContentModal(label!, 'UPDATE')}
            {modalType === 'SHOW' && (
              <ContentTableModal>
                {data &&
                  Object.keys(data[0].values).map((element, index) => {
                    return (
                      <Field
                        key={index}
                        width="400px"
                        label={defineLabel(element)}
                        name={element}
                        disabled
                        value={data[0].values[element]}
                      />
                    );
                  })}
              </ContentTableModal>
            )}
            {modalType === 'DELETE' && (
              <ContentTableModal>
                <Infos weight={500}>
                  <Title type="h2" label="⚠️" size="60px" margin="0px" />
                  Vous devez confirmer la supression avant de continuer <br /> Attention cette
                  action est irréversible
                </Infos>
                <BtnGroup>
                  <Button label="Annuler" onClick={onClose} />
                  <Button label="Confirmer" onClick={onDelete} />
                </BtnGroup>
              </ContentTableModal>
            )}
          </Modal>
        }
      </>
    );
  }
);
