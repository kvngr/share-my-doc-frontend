import React, { useReducer, useState, useEffect, memo } from 'react';
import styled, { css } from 'styled-components';
import Icon from 'src/components/atoms/Icon';
import { TableModal } from 'src/components/organisms/Table/TableModal';
import { actionsTableReducer } from './context';
import { eyeIcon, trashIcon, editIcon, addIcon } from 'src/utils/icons';
import { colors } from '@themes';
import { useUserStore } from '@core';

interface ActionsProps {
  showTableActions?: boolean;
  disabled?: boolean;
  data?: any;
  label?: string;
  onDelete: () => void;
}

const ActionsWrapper = styled.div`
  display: flex;
`;

const ButtonIcon = styled.button<{ disabled?: boolean }>`
  border: none;
  background: none;
  outline: none;
  cursor: pointer;

  ${({ disabled }) =>
    disabled &&
    css`
      cursor: not-allowed;

      * {
        color: ${colors.grey};
        opacity: 0.6;
      }
    `}
`;

export const Actions = memo(
  ({ showTableActions, disabled, data, onDelete, label }: ActionsProps) => {
    const [state, dispatch] = useReducer(actionsTableReducer, { modalType: null });
    const [isActive, setIsActive] = useState(false);
    const { isDeleted } = useUserStore();

    const { modalType } = state;

    const handleOnClose = () => {
      setIsActive(false);
      dispatch({ type: 'RESET' });
    };

    useEffect(() => {
      if (modalType !== null) {
        setIsActive(!isActive);
      }
    }, [modalType]);

    useEffect(() => {
      if (isDeleted) {
        handleOnClose();
      }
    }, [isDeleted]);

    return (
      <>
        <TableModal
          data={data}
          label={label}
          onClose={handleOnClose}
          active={isActive}
          modalType={modalType}
          onDelete={onDelete}
        />
        {showTableActions && (
          <ActionsWrapper>
            <ButtonIcon onClick={() => dispatch({ type: 'CREATE' })}>
              <Icon size="2x" icon={addIcon} color={colors.blue} />
            </ButtonIcon>
            <ButtonIcon onClick={() => dispatch({ type: 'SHOW' })} disabled={disabled}>
              <Icon size="2x" icon={eyeIcon} color={colors.grey} />
            </ButtonIcon>
            <ButtonIcon onClick={() => dispatch({ type: 'UPDATE' })} disabled={disabled}>
              <Icon size="2x" icon={editIcon} color={colors.green} />
            </ButtonIcon>
            <ButtonIcon onClick={() => dispatch({ type: 'DELETE' })} disabled={disabled}>
              <Icon size="2x" icon={trashIcon} color={colors.red} />
            </ButtonIcon>
          </ActionsWrapper>
        )}
      </>
    );
  }
);
