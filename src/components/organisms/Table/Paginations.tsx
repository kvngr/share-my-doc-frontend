import React from 'react';
import styled, { css } from 'styled-components';
import { Input } from 'src/components/atoms/Input';
import { showingOptions } from './helpers';
import { colors } from '@themes';
import './assets/styles.scss';

interface PaginationProps {
  pageIndex: number;
  pageSize: number;
  gotoPage: (updater: ((pageIndex: number) => number) | number) => void;
  previousPage: () => void;
  nextPage: () => void;
  setPageSize: (pageSize: number) => void;
  pageCount: number;
  pageOptions: number[];
  canPreviousPage: boolean;
  canNextPage: boolean;
}

const Wrapper = styled.div`
  padding: 0.5rem;
  display: flex;
  align-items: center;
  * {
    color: ${props => props.theme.color};
  }
`;

const Select = styled.select`
  font-size: 14px;
  color: ${props => props.theme.color};
  background: transparent;
  outline: none;
`;

const Btn = styled.button<{ disabled?: boolean }>`
  background: none;
  outline: none;
  border: none;
  font-size: 16px;
  cursor: pointer;
  border: 1px solid ${props => props.theme.color};
  margin: 0 5px;
  border-radius: 3px;
  color: ${props => props.theme.color};

  ${({ disabled }) =>
    !disabled &&
    css`
      &:hover {
        border-color: ${colors.primary};
        color: ${colors.primary};
      }
    `}

  ${({ disabled }) =>
    disabled &&
    css`
      color: ${colors.cloudGrey};
      border-color: ${colors.cloudGrey};
    `}
`;

const Wording = styled.span`
  margin: 0 10px;
`;

export const Pagination = ({
  gotoPage,
  previousPage,
  canPreviousPage,
  canNextPage,
  nextPage,
  setPageSize,
  pageCount,
  pageOptions,
  pageIndex,
  pageSize
}: PaginationProps) => (
  <Wrapper>
    <Btn onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
      {'<<'}
    </Btn>{' '}
    <Btn onClick={() => previousPage()} disabled={!canPreviousPage}>
      {'<'}
    </Btn>{' '}
    <Btn onClick={() => nextPage()} disabled={!canNextPage}>
      {'>'}
    </Btn>{' '}
    <Btn onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
      {'>>'}
    </Btn>{' '}
    <Wording>
      Page{' '}
      <strong>
        {pageIndex + 1} sur {pageOptions.length}
      </strong>
    </Wording>
    <Wording>Aller à la page: </Wording>{' '}
    <Input
      type="number"
      name="pageSearch"
      min="1"
      max={pageOptions.length}
      defaultValue={pageIndex + 1}
      className="page-search-table-input"
      onChange={event => {
        const page = event.target.value ? Number(event.target.value) - 1 : 0;
        gotoPage(page);
      }}
    />
    <Select
      value={pageSize}
      onChange={event => {
        setPageSize(Number(event.target.value));
      }}
    >
      {showingOptions.map(pageSize => (
        <option key={pageSize} value={pageSize}>
          Voir {pageSize}
        </option>
      ))}
    </Select>
  </Wrapper>
);
