import React, { ReactNode } from 'react';
import { Field } from 'src/components/molecules/Field';
import { Row } from 'react-table';
import './assets/styles.scss';

interface FilterProps {
  preGlobalFilteredRows: Row[];
  globalFilter: any;
  name: string | undefined;
  children?: ReactNode;
  setGlobalFilter: (value: string) => void;
}

export const GlobalFilter = ({
  preGlobalFilteredRows = [],
  globalFilter,
  name,
  children,
  setGlobalFilter
}: FilterProps) => {
  const count = preGlobalFilteredRows.length;

  return (
    <>
      <Field
        className="global-search-table-input"
        name="inputSearch"
        placeholder={`Recherche dans les ${count} ${name}`}
        value={globalFilter || ''}
        onChange={event => {
          const { value } = event.target;
          setGlobalFilter(value);
        }}
      />
      {children}
    </>
  );
};
