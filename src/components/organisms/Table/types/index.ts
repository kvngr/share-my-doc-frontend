import { UserRole } from 'src/core/types/user';

export type FormType = 'CREATE' | 'SHOW' | 'UPDATE' | 'DELETE' | 'UPLOAD';

export type UserFormData = {
  role: UserRole;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  domainId: string;
  allowedDomainIds: string;
};

export type DomainFormData = {
  name: string;
  description: string;
};

export type DocumentFormData = {
  title: string;
  rule: string;
  domainId: string;
  revision: number;
  version: number;
};

export type RulesFormData = {
  title: string;
  content: string;
  version: number;
  revision: number;
  domainId: string;
  image: string;
};
