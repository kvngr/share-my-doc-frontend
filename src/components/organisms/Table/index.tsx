import React, { useMemo, useEffect, memo } from 'react';
import { useTableStore } from '@core';
import styled, { css } from 'styled-components';
import {
  useTable,
  useFilters,
  usePagination,
  useGlobalFilter,
  Column,
  TableState,
  useRowSelect
} from 'react-table';

import { GlobalFilter } from './Filter';
import { Pagination } from './Paginations';
import { IndeterminateCheckbox } from './Checkbox';
import { getListId } from './helpers';
import { colors } from '@themes';
import { Actions } from './Actions';
import { Select } from 'src/components/molecules/Select';

interface TableProps {
  display?: string;
  templateColumn?: string;
}

interface OptionsFilter {
  label: string;
  value: string;
}

interface AbstractTableProps {
  columns: Column[];
  optionsFilter?: OptionsFilter[];
  data: any;
  label?: string;
  showTableActions?: boolean;
  initialState?: Partial<TableState<{ pageIndex: number }>>;
  onDelete: () => void;
}

export const Table = styled.div<TableProps>`
  display: ${({ display }) => display || 'grid'};
  grid-template-columns: ${({ templateColumn }) => templateColumn};
  color: ${props => props.theme.color};
  background: ${props => props.theme.backgroundColor};
`;

export const THead = styled.thead`
  .table-row-header {
    th {
      display: flex;
      align-items: center;

      .select-table-filter {
        margin: 0 0 5px 20px;
      }
    }
  }
`;

export const TRow = styled.tr<{ isSelected?: boolean; showHover?: boolean; isTRowHead?: boolean }>`
  &:hover {
    background: ${({ showHover }) => showHover && colors.primary};
    color: ${({ isTRowHead }) => !isTRowHead && colors.white};
  }
  ${({ isSelected }) =>
    isSelected &&
    css`
      background: ${colors.primary};
      color: ${colors.white};
    `};
  transition: background 0.2s linear;
`;
export const TH = styled.th<{ flex?: number }>`
  flex: ${({ flex }) => flex};
`;
export const TBody = styled.tbody``;
export const TD = styled.td``;

export const TTable = styled.table`
  /* width: 100%; */
  border-spacing: 0;
  color: ${props => props.theme.color};
  border: 1px solid ${props => props.theme.color};

  ${TRow} {
    :last-child {
      ${TD} {
        border-bottom: 0;
      }
    }
  }

  /* ${TH} {
    display: flex;
    align-items: center;
  } */

  ${TH},
  ${TD} {
    margin: 0;
    padding: 0.5rem;
    border-bottom: 1px solid ${props => props.theme.color};
    border-right: 1px solid ${props => props.theme.color};

    :last-child {
      border-right: 0;
    }
  }
`;

const TableWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 40px;
`;

const TableLabel = styled.span`
  color: ${props => props.theme.color};
  text-align: center;
  font-weight: bold;
  font-size: 22px;
  letter-spacing: 1px;
`;

const CheckboxWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const AbstractTable = memo(
  ({
    columns,
    showTableActions,
    optionsFilter = [],
    data,
    label,
    onDelete,
    initialState = { pageIndex: 0 }
  }: AbstractTableProps) => {
    const {
      getTableProps,
      selectedFlatRows,
      state: { pageIndex, pageSize, globalFilter, selectedRowIds },
      page,
      headerGroups,
      getTableBodyProps,
      canPreviousPage,
      canNextPage,
      prepareRow,
      preGlobalFilteredRows,
      setGlobalFilter,
      pageCount,
      pageOptions,
      gotoPage,
      nextPage,
      previousPage,
      setPageSize
    } = useTable(
      {
        columns,
        data,
        initialState
      },
      useFilters,
      useGlobalFilter,
      usePagination,
      useRowSelect,
      hooks => {
        hooks.flatColumns.push(columns => [
          {
            id: 'selection',
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <CheckboxWrapper>
                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
              </CheckboxWrapper>
            ),
            Cell: ({ row }) => (
              <CheckboxWrapper>
                <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
              </CheckboxWrapper>
            )
          },
          ...columns
        ]);
      }
    );

    const { setRowId, resetRowId } = useTableStore();

    const rowId = useMemo(() => getListId(selectedFlatRows, { oneId: true }), [selectedFlatRows]);

    useEffect(() => {
      if (rowId) {
        setRowId({ rowId });
      }

      return () => {
        resetRowId();
      };
    }, [rowId]);

    return (
      <TableWrapper>
        <TableLabel>{label}</TableLabel>
        {showTableActions && (
          <THead>
            <TRow className="table-row-header">
              <TH flex={1}>
                <GlobalFilter
                  name={label!.toLocaleLowerCase()}
                  preGlobalFilteredRows={preGlobalFilteredRows}
                  globalFilter={globalFilter}
                  setGlobalFilter={setGlobalFilter}
                />
                {optionsFilter && optionsFilter.length > 0 && (
                  <Select className="select-table-filter" options={optionsFilter} />
                )}
              </TH>
              <TH>
                <Actions
                  onDelete={onDelete}
                  label={label}
                  data={selectedFlatRows}
                  showTableActions
                  disabled={selectedFlatRows.length > 1 || !rowId}
                />
              </TH>
            </TRow>
          </THead>
        )}
        <TTable {...getTableProps()}>
          <THead>
            {headerGroups.map((headerGroup: any, index: any) => (
              <TRow isTRowHead key={index} {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column: any, index: any) => (
                  <TH key={index} {...column.getHeaderProps()}>
                    {column.render('Header')}
                  </TH>
                ))}
              </TRow>
            ))}
          </THead>
          <TBody {...getTableBodyProps()}>
            {page.map((row: any, index: any) => {
              prepareRow(row);
              return (
                <TRow
                  showHover
                  isSelected={selectedRowIds[index] === true}
                  key={index}
                  {...row.getRowProps()}
                >
                  {row.cells.map((cell: any, index: any) => (
                    <TD key={index} {...cell.getCellProps()}>
                      {cell.render('Cell')}
                    </TD>
                  ))}
                </TRow>
              );
            })}
          </TBody>
        </TTable>
        <Pagination
          gotoPage={gotoPage}
          previousPage={previousPage}
          canPreviousPage={canPreviousPage}
          canNextPage={canNextPage}
          nextPage={nextPage}
          setPageSize={setPageSize}
          pageCount={pageCount}
          pageOptions={pageOptions}
          pageIndex={pageIndex}
          pageSize={pageSize}
        />
      </TableWrapper>
    );
  }
);
