export type Action =
  | { type: 'RESET' }
  | { type: 'CREATE' }
  | { type: 'DELETE' }
  | { type: 'UPDATE' }
  | { type: 'SHOW' }
  | { type: null };

export interface State {
  modalType: string | null;
}

export const actionsTableReducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'RESET':
      return { modalType: null };
    case 'CREATE':
      return { modalType: 'CREATE' };
    case 'UPDATE':
      return { modalType: 'UPDATE' };
    case 'SHOW':
      return { modalType: 'SHOW' };
    case 'DELETE':
      return { modalType: 'DELETE' };
    default:
      throw new Error(`Error this ${action.type} doesn't exist`);
  }
};
