export function defineModalSize(actionType: string | null) {
  const size = { width: '50vw', height: 'auto' };

  switch (actionType) {
    case 'SHOW':
      size.width = '60vw';
      break;
    case 'DELETE':
      size.width = 'unset';
      size.height = 'unset';
      break;
    default:
      return size;
  }

  return size;
}

export function defineLabel(label: string): string {
  let value = '';

  const list = [
    {
      label: 'id',
      value: 'ID'
    },
    {
      label: 'firstName',
      value: 'Prénom'
    },
    {
      label: 'lastName',
      value: 'Nom'
    },
    {
      label: 'email',
      value: 'Adresse mail'
    },
    {
      label: 'role',
      value: 'Rôle'
    }
  ];

  list.map(element => {
    if (label === element.label) {
      value = element.value;
    }
  });

  return value;
}
