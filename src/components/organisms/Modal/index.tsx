import React, { ReactNode, useRef } from 'react';
import { createPortal } from 'react-dom';
import styled, { css } from 'styled-components';
import { fadeInAnimation, slideInAnimation } from 'src/assets/animations/animations';
import { useKeyboardEvent, useOnClickOutside } from '@hooks';
import { circleCloseIcon } from '../../../utils/icons';
import Icon from 'src/components/atoms/Icon';

interface ModalProps<T> {
  children: ReactNode;
  active: boolean;
  maxHeight?: string;
  maxWidth?: string;
  height?: string;
  width?: string;
  rounded?: boolean;
  hideCloseButton?: boolean;
  clickOverlay?: T;
  closeOnEscape?: () => void;
  closeOnClickOverlay?: () => void;
  persist?: boolean;
  onClose: () => void;
}

interface ModalWrapperProps {
  height?: string;
  width?: string;
  rounded?: boolean;
  maxWidth?: string;
  maxHeight?: string;
}

const ModalOverlay = styled.div<{ active?: boolean; clickOverlay?: any }>`
  background-color: rgba(61, 66, 86, 0.7);
  animation: ${fadeInAnimation} 0.3s cubic-bezier(0.4, 0, 0.2, 1) forwards;
  ${props =>
    props.active &&
    css`
      display: flex;
      justify-content: center;
      align-items: center;
    `}
  ${props =>
    (!props.active || props.clickOverlay) &&
    css`
      display: none;
    `}
  height: 100%;
  left: 0;
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 1000;
`;

const ModalWrapper = styled.div<ModalWrapperProps>`
  background: ${props => props.theme.backgroundColor};
  animation: ${slideInAnimation} cubic-bezier(0.4, 0, 0.2, 1) forwards;
  display: flex;
  max-height: ${props => props.maxHeight};
  max-width: ${props => props.maxWidth};
  height: ${props => props.height};
  width: ${props => props.width};
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: ${props => (props.rounded ? '30px' : '4px')};
`;

const CloseButton = styled.button`
  border: none;
  color: ${props => props.theme.color};
  background: none;
  outline: none;
  cursor: pointer;
  align-self: flex-end;
  padding: 20px;
`;

export const AbstractModal = React.memo(
  <T extends unknown>({
    active,
    children,
    rounded,
    maxWidth,
    maxHeight,
    persist,
    height,
    width,
    clickOverlay,
    closeOnEscape,
    hideCloseButton,
    closeOnClickOverlay,
    onClose
  }: ModalProps<T>) => {
    const node = useRef(null);

    const handler = !persist && closeOnClickOverlay;

    useKeyboardEvent('Escape', closeOnEscape);
    useOnClickOutside(node, handler);

    return createPortal(
      <ModalOverlay active={active} clickOverlay={clickOverlay}>
        <ModalWrapper
          ref={node}
          maxWidth={maxWidth}
          maxHeight={maxHeight}
          width={width}
          height={height}
          rounded={rounded}
        >
          {!hideCloseButton && (
            <CloseButton onClick={onClose}>
              <Icon size="2x" icon={circleCloseIcon} />
            </CloseButton>
          )}
          {children}
        </ModalWrapper>
      </ModalOverlay>,
      document.body
    );
  }
);
