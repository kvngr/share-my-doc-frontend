import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

export const Button = styled.button``;

export const FormWrapper = styled.div`
  height: 100vh;
  width: 100vw;

  @media screen and (min-width: 768px) {
    width: 500px;
    height: 500px;
  }

  margin: auto auto;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 8px;
  border: 0.8px solid rgba(0, 0, 0, 0.06);
  background: ${({ theme }) => theme.backgroundColor};
  box-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
  /* &:hover,
  &:active,
  &:focus { */
  /* border: none; */
  /* transition: box-shadow 0.2s ease-in; */
  /* } */
  /* transition: box-shadow 0.2s ease-in; */
`;

export const FormTitle = styled.h1`
  margin: 0px 45px 20px 45px;
  font-size: 25px;
  font-weight: 300;
  color: ${props => props.theme.color};
`;
