import React, { ReactNode } from 'react';
import { Field } from '../../molecules/Field';
import { Form, FormTitle } from './assets/styles';
import { Button } from '../../atoms/Button';
import { FormWrapper } from './assets/styles';

interface FormProps {
  label: string;
  onSubmit: (...args: any[]) => void;
  children: ReactNode;
}

export default ({ label, onSubmit, children }: FormProps) => (
  <FormWrapper>
    <Form onSubmit={onSubmit}>
      <FormTitle>{label}</FormTitle>
      {children}
    </Form>
  </FormWrapper>
);
