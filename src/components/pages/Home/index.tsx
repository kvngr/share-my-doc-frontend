import React from 'react';
import { PageTemplate } from '../../templates/PageTemplate';
import { Header } from '../../organisms/Header/index';
import { Footer } from '../../organisms/Footer';

const Home = () => {
  return (
    <PageTemplate>
      <div>HELLO HOME</div>
    </PageTemplate>
  );
};

export default Home;
