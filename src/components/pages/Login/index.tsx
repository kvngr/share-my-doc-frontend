import React, { useEffect } from 'react';
import useForm from 'react-hook-form';
import { RouteComponentProps } from 'react-router-dom';
import { PageTemplate } from '../../templates/PageTemplate';
import Form from '../../organisms/Form';
import { useAuthStore } from '@core';
import { Credentials } from 'src/core/types/user';
import { Button } from 'src/components/atoms/Button';
import { Field } from 'src/components/molecules/Field';
import Validator from 'src/utils/validator/Validator';
import { errorsList } from 'src/utils/errors';
import { useRedirect } from 'src/core/hooks/useRedirect';

interface LoginProps extends RouteComponentProps<any> {}

const validator = new Validator();

const Login = ({ history }: LoginProps) => {
  const defaultValues = {
    email: '',
    password: ''
  };

  const {
    handleSubmit,
    watch,
    setValue,
    register,
    unregister,
    errors,
    setError,
    formState,
    clearError
  } = useForm<Credentials>({
    mode: 'onChange',
    validateCriteriaMode: 'all',
    defaultValues
  });

  const { actions, isLoading } = useAuthStore();

  const onSubmit = (values: Credentials) => {
    actions.signIn(values);
  };

  const isAuth = useRedirect();

  useEffect(() => {
    return () => isAuth;
  }, [isAuth]);

  useEffect(() => {
    return () => {
      unregister('email');
      unregister('password');
    };
  }, [register]);

  const emailValue = watch('email');
  const passwordValue = watch('password');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setValue(event.target.name as 'email' | 'password', event.target.value);

  return (
    <PageTemplate>
      <Form onSubmit={handleSubmit(onSubmit)} label="Connectez vous">
        <Field
          name="email"
          ref={register({
            validate: value => validator.emailIsValid(value) || errorsList.input.email
          })}
          type="text"
          label="Votre addresse mail"
          onBlur={() => setError('email', 'invalidEmail', errorsList.input.email)}
          onFocus={() => clearError('email')}
          onChange={handleChange}
          error={errors.email && errors.email.message}
          value={emailValue}
        />
        <Field
          name="password"
          ref={register({
            validate: value => validator.valueIsFilled(value) || errorsList.generic.emptyField
          })}
          type="password"
          label="Votre mot de passe"
          onBlur={() => setError('password', 'invalidPassword', errorsList.generic.emptyField)}
          onFocus={() => clearError('password')}
          onChange={handleChange}
          error={errors.password && errors.password.message}
          value={passwordValue}
        />
        <Button disabled={!formState.isValid} label="Se connecter" busy={isLoading} />
      </Form>
    </PageTemplate>
  );
};

export default Login;
