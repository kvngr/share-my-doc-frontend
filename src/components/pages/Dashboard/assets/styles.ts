import styled from 'styled-components';

export const WrapperDashboard = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  margin: 20px;
  justify-content: center;
`;
