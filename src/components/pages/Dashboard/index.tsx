import React, { useMemo, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { useUserStore, useTableStore, useDomainStore, useDocumentStore, useRuleStore } from '@core';
import { PageTemplate } from '../../templates/PageTemplate';
import { Card } from 'src/components/fragment';
import { renderColumns } from 'src/components/organisms/Table/helpers';
import { AbstractTable as Table } from 'src/components/organisms/Table';
import { WrapperDashboard } from './assets/styles';
import { Header } from 'src/components/organisms/Header';
import { UserRole } from 'src/core/types/user';

interface DashboardProps extends RouteComponentProps<any> {}

const Dashboard = ({ match, history, location }: DashboardProps) => {
  const { user, users, isLoaded: usersIsLoaded, actions: userActions } = useUserStore();
  const { domain, domains, isLoaded: domainsIsLoaded, actions: domainActions } = useDomainStore();
  const {
    document,
    documents,
    isLoaded: documentsIsLoaded,
    actions: documentActions
  } = useDocumentStore();
  const { rules, rule, isLoaded: rulesIsLoaded, actions: ruleActions } = useRuleStore();

  const { rowId } = useTableStore();
  const id = rowId;
  const userRole = UserRole.USER;

  // const { state } = useContext(TableContext);

  useEffect(() => {
    userActions.findUsers();
    domainActions.findDomains();
    documentActions.findDocuments();
    ruleActions.findRules();
  }, []);

  useEffect(() => {
    userActions.findUsers();
  }, [user, users]);

  useEffect(() => {
    documentActions.findDocuments();
  }, [document, documents]);

  useEffect(() => {
    domainActions.findDomains();
  }, [domain, domains]);

  useEffect(() => {
    ruleActions.findRules();
  }, [rule, rules]);

  const userColumns = useMemo(() => renderColumns(users), [users]);
  const domainColumns = useMemo(() => renderColumns(domains), [domains]);
  const documentColumns = useMemo(() => renderColumns(documents), [documents]);
  const ruleColumns = useMemo(() => renderColumns(rules), [rules]);

  const options = [
    { label: 'ARCHIVED', value: 'ARCHIVÉS' },
    { label: 'PUBLISHED', value: 'PUBLIÉS' }
  ];

  // const optionsFilter = useMemo(() => () => options, [options]);

  const handleDeleteOneUser = () => userActions.deleteOneUser({ id });
  const handleDeleteOneDomain = () => domainActions.deleteOneDomain({ id });
  const handleDeleteOneDocument = () => documentActions.deleteOneDocument({ id });
  const handleDeleteOneRule = () => ruleActions.deleteOneRule({ id });

  return (
    <>
      <PageTemplate
        header={<Header history={history} location={location} match={match} userRole={userRole} />}
      >
        <WrapperDashboard>
          <Card>
            {usersIsLoaded && (
              <Table
                showTableActions
                label="Utilisateurs"
                columns={userColumns}
                onDelete={handleDeleteOneUser}
                data={users}
              />
            )}
          </Card>
        </WrapperDashboard>
        <WrapperDashboard>
          <Card>
            {domainsIsLoaded && (
              <Table
                showTableActions
                label="Domaines"
                columns={domainColumns}
                onDelete={handleDeleteOneDomain}
                data={domains}
              />
            )}
          </Card>
        </WrapperDashboard>
        <WrapperDashboard>
          <Card>
            {documentsIsLoaded && (
              <Table
                showTableActions
                label="Documents"
                columns={documentColumns}
                onDelete={handleDeleteOneDocument}
                data={documents}
                optionsFilter={options}
              />
            )}
          </Card>
        </WrapperDashboard>
        <WrapperDashboard>
          <Card>
            {rulesIsLoaded && (
              <Table
                showTableActions
                label="Régles"
                columns={ruleColumns}
                onDelete={handleDeleteOneRule}
                data={rules}
              />
            )}
          </Card>
        </WrapperDashboard>
      </PageTemplate>
    </>
  );
};

export default Dashboard;
