import styled from 'styled-components';

export const WrapperProfile = styled.div`
  margin: 80px;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
