import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import useForm from 'react-hook-form';
import { UserRole } from 'src/core/types/user';
import { PageTemplate } from '../../templates/PageTemplate';
import { Field } from '../../molecules/Field';
import { useAuthStore } from '@core';
import { Validator } from '../../../utils/validator/Validator';
import { dateFormat, getStringFromArray } from 'src/utils/helpers/format';
import { Table } from '../../organisms/Table';
import { Column } from 'src/components/molecules/Column';
import { Button } from 'src/components/atoms/Button';
import errorsList from 'src/utils/errors';
import { AbstractForm } from 'src/components/fragment';
import { Header } from 'src/components/organisms/Header';
import { WrapperProfile } from './assets/styles';
import './assets/styles.scss';

interface ProfileProps extends RouteComponentProps<any> {}

const validator = new Validator();

export const Profile = ({ match, history, location }: ProfileProps) => {
  const { user } = useAuthStore();
  const userRole = UserRole.USER;

  const {
    register,
    unregister,
    errors,
    watch,
    setValue,
    setError,
    clearError,
    formState,
    handleSubmit
  } = useForm({
    mode: 'onChange'
    // validateCriteriaMode: 'all'
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setValue(event.target.name as 'password', event.target.value);

  const onSubmit = (values: any) => {
    // console.log('TCL: onSubmit -> values', values);
    // actions.signIn(values);
  };

  useEffect(() => {
    return () => {
      unregister('password');
    };
  }, [register]);

  const passwordValue = watch('password');

  return (
    <PageTemplate
      header={<Header history={history} location={location} match={match} userRole={userRole} />}
    >
      <WrapperProfile>
        <AbstractForm className="form-profile" onSubmit={handleSubmit(onSubmit)}>
          <Table templateColumn="auto auto">
            {user && (
              <>
                <Column>
                  <Field name="id" label="ID" value={user.id} disabled />
                  <Field name="role" label="Rôle" value={user.role} disabled />
                  <Field name="firstName" label="Prénom" value={user.firstName} disabled />
                  <Field name="lastName" label="Nom" value={user.lastName} disabled />
                  <Field name="mail" label="Adresse email" value={user.email} disabled />
                  <Field
                    name="password"
                    label="Modifier votre mot de passe"
                    value={passwordValue}
                    onBlur={() =>
                      setError('password', 'invalidPassword', errorsList.generic.emptyField)
                    }
                    onFocus={() => clearError('password')}
                    onChange={handleChange}
                    ref={register({
                      validate: value =>
                        validator.valueIsFilled(value) || errorsList.generic.emptyField
                    })}
                    error={errors.password && errors.password.message}
                  />
                </Column>
                <Column>
                  <Field
                    name="__typename"
                    label="Type utilisateur"
                    value={user.__typename}
                    disabled
                  />
                  <Field
                    name="domain"
                    label="Domaine principal"
                    value={user && user.domain.name}
                    disabled
                  />
                  <Field
                    name="allowedDomains"
                    label="Domaines autorisés"
                    value={getStringFromArray(user.allowedDomains, 'name')}
                    disabled
                  />
                  <Field
                    name="createdAt"
                    label="Ajouté le"
                    value={dateFormat(user.createdAt as string)}
                    disabled
                  />
                  <Field
                    name="updatedAt"
                    label="Mis à jour le"
                    value={dateFormat(user.updatedAt as string)}
                    disabled
                  />
                </Column>
              </>
            )}
          </Table>
          <Button type="submit" label="Enregistrer" disabled={!formState.isValid} />
        </AbstractForm>
      </WrapperProfile>
    </PageTemplate>
  );
};

export default Profile;
