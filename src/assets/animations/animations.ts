import { keyframes } from 'styled-components';

export const fadeInAnimation = keyframes`
0% {
  opacity: 0;
}
100% {
  opacity: 1;
}
`;

export const slideInAnimation = keyframes`
  0% {
    bottom: -100%;
  }
  100% {
    bottom: 0;
  }
`;
